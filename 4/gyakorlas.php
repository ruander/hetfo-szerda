<?php
//11. Készítsünk programot, amely beolvas egy N természetes számot, majd billentyűzetről bekér N darab. természetes számot és ezeket a számokat összeadja, majd kiírja az eredményt. (Vegyünk egy változót, amit a program elején kinullázunk. Ehhez a cikluson belül mindig adjuk hozzá az éppen beolvasott számot. A szám beolvasása a ciklusban lehet N-szer ugyanabba a változóba, hiszen miután hozzáadtuk az összeghez, már nincs rá szükségünk, tehát használhatjuk a következő szám beolvasására.) - step 1 - hány darab számot adjunk össze - step 2 - annyi input mező kirakása, amennyi szükséges
//lépések tárolása
$step = 1;
if (!empty($_POST)) {
    //echo '<pre>' . var_export($_POST, true) . '</pre>';
    //hibakezelés
    $hiba = [];
    $options = [
        'options'=> [
            'min_range' => 2
        ]
    ];
    $n = filter_input(INPUT_POST, 'n', FILTER_VALIDATE_INT,$options);
    //die($n);
    if ($n < 2) {//a feladatra értelmezve
        $hiba['n'] = '<span class="error">Nem érvényes formátum!</span>';
    }else{
        $step = 2;
    }
    //összeadandó számok hibakezelése (tömb) filter_input_array paraméterezéssel
    $args = [
        'm' => [
            /*'filter'=> FILTER_VALIDATE_INT,
            'options'=> [
                'min_range' => 2
            ],*/
            'flags' => FILTER_REQUIRE_ARRAY
        ]
    ];
    $test = filter_input_array(INPUT_POST,$args);
//https://www.php.net/manual/en/function.filter-input-array.php
    //bejárás-hibakezelés ha a kapott szűrt elem tömb
    if(is_array($test['m'])){

       foreach($test['m'] as $k =>$v){
            if(filter_var($v,FILTER_VALIDATE_INT) === false){//itt szűrünk egész számra
                $hiba['m'][$k]='<span class="error">Nem érvényes formátum!</span>';
            }
       }
       //step 2 ben nincs hiba
        if (empty($hiba)) {

            //elemek összege
            $sum  = array_sum($test['m']);
            //a válasz kialakítása
            $output = '<h3>A számok és az eredmény</h3>';
            // 1 + 2 + 4 + (-5) = 2
            $output .= implode(' + ',$test['m']).' = '.$sum;
            $output .= '<div><a href="'.$_SERVER['PHP_SELF'].'">Újra</a></div>';//https://www.php.net/reserved.variables.server
            //die();
        }
    }

    //echo '<pre>' . var_export($_SERVER, true) . '</pre>';
    //echo '<pre>' . var_export($hiba, true) . '</pre>';

}

if(isset($output)){//ha van output akkor van eredmény, egyébként űrlap
    echo $output;
}else {
//űrlap kialakítása
    $form = '<h2>Számok összeadása (11. feladatgyüjtemény példa)</h2>
            <form method="post">';
//megjelenés szétválasztása
    switch ($step) {
        case 2:
            $form .= 'Öszzeadás elemei:';
            //mezők kihelyezése ciklus segítségével
            for ($i = 1; $i <= $n; $i++) {
                $form .= '<label>Szám ' . $i . ':
                <input type="text" name="m[' . $i . ']" maxlength="5" value="';
                if (isset($test['m'][$i])) {//ha létezik az adott érték, befűzzük az ürlapba
                    $form .= $test['m'][$i];
                }
                $form .= '">';

                //hiba kiírása ha van, belefűzzük az ürlapba
                if (isset($hiba['m'][$i])) {
                    $form .= $hiba['m'][$i];//$form = $form.$hiba['n'];
                }

                $form .= '</label>';

            }
            //rejtett input mező az n tárolására
            $form .= '<input type="hidden" name="n" value="' . $n . '">';
            break;
        default:
            $form .= '<label>Hány számot adjunk össze (min:2)
                <input type="text" size="2" name="n" maxlength="2" value="' . filter_input(INPUT_POST, 'n') . '">';

            //hiba kiírása ha van, belefűzzük az ürlapba
            if (isset($hiba['n'])) {
                $form .= $hiba['n'];//$form = $form.$hiba['n'];
            }

            $form .= '</label>';
            break;
    }
    $form .= '<button>Mehet</button>
</form>';
    echo $form;
}
//stílusok
$style = '<style>
    form {
        max-width:450px;
        display:flex;
        flex-flow:column nowrap;
    }
    .error {
        color:red;
        font-size:13px;
        font-style: italic;
    }
</style>';
//stílusok kiírása
echo $style;
/**
 * @todo: HF - 2 lépcsős űrlap - első lépcső hány mező legyen (5, 6 vagy 7 a helyes megoldás) - select option, vagy number input, mindegy
 *step 2: 5 6 vagy 7 mezőből adatbekérés
 *  - 5 esetén 1 - 90 között
 *  - 6 esetén 1 - 45 között
 *  - 7 esetén 1 - 35 között
 hibátlan kitöltés esetén felirat_
 * Tippsor: 1,3,14,25,45,67 (6esetén minta) (növekvő értékek szerint)
 * azonos értékek nem hibák egyenlőre
 */