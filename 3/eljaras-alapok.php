<?php

echo implode(',',dobas(3, 10));


/*
 function eljarasneve([param1,param2 = érték, ...]){
    //programkód

    visszatérés:
    return érték;
}
 */

/**
 * Saját eljárás kockadobás generlásra
 * @param int $db | dobások száma
 * @param int $oldalak_szama / hány oldalú 'kockával'
 * @return array
 */
function dobas($db = 1, $oldalak_szama = 6){
    $dobasok=[];
    for($i=1;$i<=$db;$i++){
        $dobasok[]=rand(1,$oldalak_szama);
    }
    return $dobasok;
}