<?php
//kapott adatok feldolgozása
//ha kapunk adatot, akkor nem üres a POST
if (!empty($_POST)) {
    echo '<pre>A POST szuperglobális tömbböl érkező adatok: ' . var_export($_POST, true) . '</pre>';
    //hibakezelés
    $hiba = [];//üres hiba tömb
    //név min 3 karakter
    $name = filter_input(INPUT_POST, 'name');//INPUT_POST -> rendszerállandó
    //eleje és vége spacek eltávolítása
    $name = trim($name);//php.net trim,ltrim,rtrim
    if (mb_strlen($name, "utf-8") < 3) {
        $hiba['name'] = '<span class="error">minimum 3 karakter!</span>';
    }
    //email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="error">Nem érvényes formátum!</span>';
    }
    //jelszavak
    $pass = filter_input(INPUT_POST, 'pass');
    $repass = filter_input(INPUT_POST, 'repass');
    //min 6 kar
    if (mb_strlen($pass, 'utf-8') < 6) {
        $hiba['pass'] = '<span class="error">Jelszó min 6 karakter!</span>';
    } elseif ($pass !== $repass) {
        $hiba['repass'] = '<span class="error">A jelszavak nem egyeztek!</span>';
    } else {
        //a jelszó rendben
        //echo $pass = md5($pass);//md5 hash, nem túl biztonságos hashalési technológia
        //vagy mégis?
        //$secret= "s3Cr3T_k3Y!";
        for ($i = 1; $i <= 100; $i++) {
            $pass = md5($pass);
        }
        echo $pass;
    }


    if (empty($hiba)) {//ha nincs hiba üres marad a hibatömb
        //jók az adatok
        $user = [
            'name' => $name,
            'email' => $email,
            'password' => $pass,
        ];
        die('<br>az adatok jók:'.implode(',',$user));
    }
}

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP tanfolyam - űrlapok - felhasználó regisztrációs űrlap</title>
    <style>
        body, html {
            margin: 0;
            padding: 0;
            font-family: Arial, sans-serif;
        }

        form {
            display: block;
            padding: 5px;
        }

        label {
            display: block;
            margin: 5px 0;
        }

        .error {
            color: red;
            font-style: italic;
            font-size: 13px;
        }
    </style>
</head>
<body>
<h1>Regisztráció</h1>
<form method="post">
    <label>
        Név: <input type="text" name="name" autocomplete="off" placeholder="John Doe"
                    value="<?php echo filter_input(INPUT_POST, 'name') ?>">
        <?php
        //mezőhiba kiírás, ha van
        if (isset($hiba['name'])) {
            echo $hiba['name'];
        }
        ?>
    </label>
    <label>
        Email: <input type="text" name="email" placeholder="john@doe.email"
                      value="<?php echo filter_input(INPUT_POST, 'email') ?>">
        <?php
        //mezőhiba kiírás, ha van
        if (isset($hiba['email'])) {
            echo $hiba['email'];
        }
        ?>
    </label>
    <label>
        Jelszó: <input type="password" name="pass">
        <?php
        //mezőhiba kiírás, ha van
        if (isset($hiba['pass'])) {
            echo $hiba['pass'];
        }
        ?>
    </label>
    <label>
        Jelszó újra: <input type="password" name="repass">
        <?php
        //mezőhiba kiírás, ha van
        if (isset($hiba['repass'])) {
            echo $hiba['repass'];
        }
        ?>
    </label>
    <button>Gyííí</button>
</form>
</body>
</html>