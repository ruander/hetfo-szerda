<?php
//kapott adatok feldolgozása
//
echo '<pre>A POST szuperglobális tömbböl érkező adatok: '.var_export($_POST,true).'</pre>';
//ezt így nem illik használni: $_POST['name'] ,helyette filter_input()
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP tanfolyam - űrlapok 1</title>
    <style>
        body, html {
            margin: 0;
            padding: 0;
            font-family: Arial,sans-serif;
        }
        form {
            display: block;
            padding: 5px;
        }
        label {
            display: block;
            margin: 5px 0;
        }
    </style>
</head>
<body>
<h1>Get metódus</h1>
<form method="get" action="feldolgoz.php">
    <label>
        Név: <input type="text" name="name" autocomplete="off">
    </label>
    <label>
        Email: <input type="text" name="email">
    </label>
    <button>Gyííí</button>
</form>
<h1>Post metódus - self process</h1>
<form method="post">
    <label>
        Név: <input type="text" name="name" autocomplete="off">
    </label>
    <label>
        Email: <input type="text" name="email">
    </label>
    <button>Gyííí</button>
</form>

</body>
</html>