<?php
//Töltsünk fel egy tömböt 15-25 közötti véletlen számokkal 20 elem
$tomb = [];//ide gyűjtjük a számokat
for ($i = 0; $i < 20; $i++) {
    $tomb[] = rand(15, 25);
}
echo '<pre>' . var_export($tomb, true) . '</pre>';
//nézzük meg  hány db 20nál nagyobb illetve kisebb érték van, és mennyi 20as
//tömb bejárása
/*
foreach($tomb as $key => $value){
    //ciklusmag $key és $value elérhető
}
 */
$kisebb = 0;
$nagyobb = 0;
$huszas = 0;
foreach ($tomb as $k => $v) {
    echo "<br>kulcs: $k | érték: $v";
    if ($v < 20) {
        $kisebb++;
    } elseif ($v > 20) {
        $nagyobb++;
    } else {
        $huszas++;
    }//if($v<20) vége
}//ciklus vége

echo "<div>20nál kisebb: $kisebb | 20nál nagyobb: $nagyobb | huszas: $huszas</div>";
//több dimenziós tömb
$users = [
    1 => [
        'email' => 'hgy@ruander.hu',
        'name' => 'Horváth György',
        'regDate' => '2020-04-15 17:34:00'
    ],
    2 => [
        'email' => 'test@email.cim',
        'name' => 'Teszt Elek',
        'regDate' => date('Y-m-d H:i:s')
    ],
    3 => 'helloworld'
    //...
];
echo '<pre>' . var_export($users, true) . '</pre>';
//több dimenzió esetén a tömb érték ellenőrizendő
foreach ($users as $id => $user){
    //echo "<br>id: $id | {$user['name']} ";//tömb  kulcsról primitiv  kiírása "" között {}

    echo '<br>';
    if(!is_array($user)){//is_array($user) === false
        echo 'primitiv';
    }else{
        echo 'tömb';
        echo '<br>Felhasználó ('.$id.'): '.implode(' | ',$user);
    }
}
//tömb elemszám
echo '<br>Összes felhasználó szám: '.count($users);
