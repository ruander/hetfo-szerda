<?php
$szoveg = '<p>Ez egy teszt szöveg [!amiben vannak számok!] is 123, 456, 233 , és speciális karakterek, mint pl %3 [] és {} jelek.</p><p>Html <b>elemeket</b> is tartalmaz!</p>';
echo $szoveg;
$szovegHossz = mb_strlen($szoveg,'utf-8');//szöveg kódlaphelyes hossza
$tagekNelkul = strip_tags($szoveg);//html és php tagek eltávolítása
var_dump('<pre>',$szoveg);
echo $pozicio = strpos($szoveg,'<b>');//szöveg előfordulásának az első eleme
//szöveg csere
$mit = 'elemeket';
$mire = 'TAGeket';
$modosultSzoveg = str_replace($mit,$mire,$szoveg);
echo $modosultSzoveg;
$mit = ['[!','!]'];
$mire = ['<i>','</i>'];
$modosultSzoveg = str_replace($mit,$mire,$szoveg);
echo $modosultSzoveg;
$repeat = 'bla... ';
echo str_repeat($repeat,6);
echo mb_strtoupper($szoveg,'utf-8');
echo mb_strtolower($szoveg,'utf-8');
echo wordwrap($szoveg,60,'...');
$szavakSzama = str_word_count($tagekNelkul);
echo "<h2>A szavak száma : $szavakSzama</h2>";
$quiz = "Megrengeti";
//betűk összekeverése
$betuk = str_split(mb_strtoupper($quiz,'utf-8'),1);//tömb annyi elemmel ahány betűre bontjuk
var_dump($betuk);
//véletlen sorrend
shuffle($betuk);
var_dump($betuk);
$kitalalando = $betuk;
echo $feladat = '<h2>Milyen szó betűit kevertük össze? <br>'.implode("",$betuk).'</h2>';
//lehessen több kitalálandó
$quizElemek = [
  'Valami','Találdki','Troll',"Megrengeti",'Ruander', 'PHP'
];
//egy véletlen elem kell ebből a tömbből
//echo $quiz = $quizElemek[array_rand($quizElemek)];
//másik módszer eddig tanult elemekből
$tombHossz = count($quizElemek);
$veletlenSzam = rand(0,$tombHossz-1);
echo $quiz = $quizElemek[$veletlenSzam];
//echo $quizElemek[rand(0,count($quizElemek)-1)];//egybeszuszakolva ugyanaz
//betűk összekeverése
$betuk = str_split(mb_strtoupper($quiz,'utf-8'),1);//tömb annyi elemmel ahány betűre bontjuk
//véletlen sorrend
shuffle($betuk);
echo $feladat = '<h2>Milyen szó betűit kevertük össze? <br>'.implode("",$betuk).'</h2>';
//
echo $code = md5(time());
//a kód minden 6. karaktere után legyen kötőjel (kivéve ha a végére esik, akkor nem kell)
$parts = str_split($code,6);
$ujcode = implode('-',$parts);
echo "<br>$ujcode";
//kis olvasni való hasznos anyag, sok helyen alkalmazzák a logikáját
//https://www.php.net/manual/en/reference.pcre.pattern.syntax.php
//preg_match(),preg_match_all(), preg_replace(), ... és még sok eljárás alkalmazza a pattern logikát