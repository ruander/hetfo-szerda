<?php
//mappa műveletek
//echo DIRECTORY_SEPARATOR;//rendszer mappa elválasztó, pl windows \ , linux /
$dir = 'teszt/';//mappa neve, a végén lehet /
//var_dump('<pre>',is_dir($dir));//csak mappa tipus
//var_dump(is_file($dir));//csak file tipus
//var_dump(file_exists($dir));//mindegy, hogy mappa vagy file tipus
if(!is_dir($dir)){
    //elkészítjük a mappát
    mkdir($dir,0755, true);//rekurzív mód ha több mappa is hiányzik
}
$mappaLista = array_diff(scandir($dir),['.','..']);//a . és .. kivonása a kapott mappalistából
//var_dump($mappaLista);
if(empty($mappaLista)){
    echo '<div>A mappa üres</div>';
}else{
    //soroljuk fel a tartalmat
    $lista = '<ul>';
    foreach($mappaLista as $elem){
        //irjuk mellé a tipusát (shorten if)
        $tipus = is_dir($dir.$elem)?'mappa':'file';
        $lista .= "<li>$elem - $tipus</li>";
    }
    $lista .= '</ul>';
    echo $lista;
}
//ezekkel az adatokkal dolgozunk:
$user = [
  'id' => 1,
  'email' => 'hgy@iworkshop.hu',
  'password' => password_hash('123456',PASSWORD_BCRYPT)
];
//átalakítás primitiv (tárolható) formátummá - string
$userJson = json_encode($user);
//2
$userSor = serialize($user);
//var_dump($user,$userJson,$userSor);
//visszakapjuk ugyanazt?
//var_dump(json_decode($userJson,true),unserialize($userSor));
//irjuk ki fileba a json adatokat: teszt/user.json, és teszt/user.ser
$fileNameJson = 'user.json';
$fileNameSor = 'user.ser';
file_put_contents($dir.$fileNameJson,$userJson);
file_put_contents($dir.$fileNameSor,$userSor);

//adatok visszaolvasása a fileból
//jsonbol
$contentJson = file_get_contents($dir.$fileNameJson);
var_dump('<pre>',json_decode($contentJson,true));
//serialbol
$contentSor = file_get_contents($dir.$fileNameSor);
var_dump(unserialize($contentSor));
//url
//var_dump(file_get_contents('https://ruander.hu'));
//mappa törlése
//törölni csak üres mappát lehet
$almappa = $dir.'almappa/';
//van mappa nincs mappa
if(is_dir($almappa)){//ha létezik töröljük
    $almappaLista = array_diff(scandir($almappa),['.','..']);//ha üres akkor lehet törölni
    if(empty($almappaLista)) {
        rmdir($almappa);//ha nem üres a mappa hibát ad
    }
}else{
    //vagy létrehozzuk
    mkdir($almappa,0755,true);
}
//file törlése
//ha van file trörljük, ha nincs létrehozunk egyet
//var_dump(unlink($dir));
$testFile = $almappa.'tesztfile.txt';//ezt keressük
if(is_file($testFile)){
    unlink($testFile);
}else{
    //ha nincs almappa, csináljunk
    makeMyDir($almappa);
    touch($testFile);//ha nincs létrehoz üres filet, ha van módosítja az időbélyeget
}
/**
 * Saját mappa ellenőrző létrehozó eljárás
 * @param $dir
 * @param int $mode
 * @param void
 */
function makeMyDir($dir,$mode = 0755, $recursive = true){
    if(!is_dir($dir)) mkdir($dir,$mode,$recursive);
    return;
}