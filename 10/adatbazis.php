<?php
$dbHost = 'localhost';//adatbázis szerver címe
$dbUser = 'root';//adatbázis felhasználó
$dbPass = '';//adatbázis felhasználó jelszava
$dbName = 'classicmodels';//használt adatbázis neve
//csatlakozás vagy állj
$link = @mysqli_connect($dbHost,$dbUser,$dbPass,$dbName) or die('db hiba: '.mysqli_connect_error());
//minta lekérés, összes alkalmazott neve mysqli_query(link, query)
$qry = "SELECT CONCAT(firstname,' ',lastname) `fullname`,officeCode FROM employees";//kérés összeállítása
$result = mysqli_query($link,$qry) or die(mysqli_error($link));//lekérés vagy állj a hibával - az eremény egy halmaz lesz, maga a result egy resource
//var_dump('<pre>',$result);
/*
$row = mysqli_fetch_row($result);//egy sor kibontása automatikus indexelt tömbbe
var_dump('<pre>',$row);
$row = mysqli_fetch_assoc($result);//egy sor kibontása asszociativan indexelt tömbbe, kulcsok a mezőnevek lesznek
var_dump($row);
$row = mysqli_fetch_array($result);//egy sor kibontása automatikus indexelt tömbbe és asszociativba
var_dump($row);
$row = mysqli_fetch_object($result);//egy sor kibontása automatikus objektumba
var_dump($row);
$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);//összes sor kibontása automatikus indexelt tömbbe
var_dump($rows);
*/
//sorok kibontása ciklusban
echo '<h3>Dolgozók nevei és irodakódjai</h3>';
echo '<ul>';
while(null !== $row = mysqli_fetch_assoc($result)){
    //sorok adatai
    //echo '<pre>'.var_export($row,true).'</pre>';
    echo "<li><b>{$row['fullname']}</b> - {$row['officeCode']}</li>";
}
echo '</ul>';