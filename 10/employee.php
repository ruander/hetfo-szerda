<?php
//űrlap kezelése
if (!empty($_POST)) {
    $hiba = [];
//employeenumber, szám kell legyen (később egyedi kell legyen)
    $employeeNumber = filter_input(INPUT_POST, 'employeeNumber', FILTER_VALIDATE_INT);
    if ($employeeNumber < 1) {
        $hiba['employeeNumber'] = '<span class="error">Nem érvényes formátum!</span>';
    }
//firstname, (ne maradjon üres)
    $firstName = filter_input(INPUT_POST, 'firstName');
    if ($firstName == '') {
        $hiba['firstName'] = '<span class="error">Kötelező kitölteni!</span>';
    }
//lastname, (ne maradjon üres)
    $lastName = filter_input(INPUT_POST, 'lastName');
    if ($lastName == '') {
        $hiba['lastName'] = '<span class="error">Kötelező kitölteni!</span>';
    }

//extension, -
    $extension = filter_input(INPUT_POST, 'extension');
//email, - legyen email, (később, egyediség vizsgálata)
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="error">Nem érvényes formátum!</span>';
    }
//officecode, szám kell legyen (jelenleg 1-8 közötti, később, db ből lenyilo)
    $options = [
        'options' => [
            'min_range' => 1,
            'max_range' => 8
        ]
    ];//filterhez opciók
    $officeCode = filter_input(INPUT_POST,'officeCode',FILTER_VALIDATE_INT,$options);
    if(!$officeCode){
        $hiba['officeCode'] = '<span class="error">Nem érvényes formátum!</span>';
    }
//reportsto, - szám kell legyen (később: szerepelnie kell már, vagy lehet üres is , akkor főnök)
    $reportsTo = filter_input(INPUT_POST, 'reportsTo', FILTER_VALIDATE_INT);
    if ($reportsTo < 1) {
        //@todo: itt az üres mező jó érték kéne legyen, ha van beírva valami akkor viszont az szám kell legyen ami 0 nál nagyobb
        $hiba['reportsTo'] = '<span class="error">Nem érvényes formátum!</span>';
    }

//jobtitle -
    $jobTitle = filter_input(INPUT_POST, 'jobTitle');

    echo '<pre>' . var_export($hiba, true) . '</pre>';
    if (empty($hiba)) {
        //adatok rendberakása eg ykönnyen kezelhető tömbbe
        $employee = [
          'employeeNumber' => $employeeNumber,
          'firstName' => $firstName,
          'lastName' => $lastName,
          'email' => $email,
          'officeCode' => $officeCode,
          'extension' => $extension,
          'reportsTo' => $reportsTo,
          'jobTitle' => $jobTitle
        ];
        //minden ok, mehet a művelet, most adatbázisba szeretnénk illeszteni
        $dbHost = 'localhost';//adatbázis szerver címe
        $dbUser = 'root';//adatbázis felhasználó
        $dbPass = '';//adatbázis felhasználó jelszava
        $dbName = 'classicmodels';//használt adatbázis neve
//csatlakozás vagy állj
        $link = @mysqli_connect($dbHost,$dbUser,$dbPass,$dbName) or die('db hiba: '.mysqli_connect_error());

        //echo '<pre>' . var_export($employee, true) . '</pre>';
        /*$qry = "INSERT INTO
        `employees` 
        (
            `employeeNumber`, 
            `lastName`, 
            `firstName`, 
            `extension`, 
            `email`, 
            `officeCode`, 
            `reportsTo`, 
            `jobTitle`
        ) 
        VALUES (
            '{$employee['employeeNumber']}', 
            '{$employee['lastName']}', 
            '{$employee['firstName']}', 
            '{$employee['extension']}', 
            '{$employee['email']}', 
            '{$employee['officeCode']}', 
            '{$employee['reportsTo']}', 
            '{$employee['jobTitle']}'
        )";*/
        echo $qry = "INSERT INTO employees(`".implode('`,`',array_keys($employee))."`)
                VALUES('".implode("','",$employee)."')";
        //kérés futtatása
        mysqli_query($link,$qry) or die(mysqli_error($link));
        die('ok');
    }
}
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Új alkalmazott felvitele</title>
    <style>
        html, body {
            padding: 0;
            margin: 0;
        }

        label {
            padding: 5px 0;
        }

        form {
            max-width: 500px;
            margin: 0 auto;
            display: flex;
            flex-flow: column nowrap;
        }

        h1 {
            font-family: Arial, sans-serif;
            text-align: center;
        }
        .error {
            font-size:12px;
            color:red;
            font-style: italic;
        }
    </style>
</head>
<body>
<h1>Új alkalmazott felvitele</h1>
<form method="post">
    <!--employeenumber,firstname,lastname,extension,email,officecode,reportsto,jobtitle-->
    <label>
        employeenumber<sup>*</sup> <input type="text" name="employeeNumber" value="<?php echo inputValue('employeeNumber') ?>" placeholder="5000">
        <?php
        //mezőhiba
        echo hibaKiir('employeeNumber');
        ?>
    </label>
    <label>
        firstname<sup>*</sup> <input type="text" name="firstName" value="<?php echo inputValue('firstName') ?>" placeholder="John">
        <?php
        //mezőhiba
        echo hibaKiir('firstName');
        ?>
    </label>
    <label>
        lastname<sup>*</sup> <input type="text" name="lastName" value="<?php echo inputValue('lastName') ?>" placeholder="Doe">
        <?php
        //mezőhiba
        echo hibaKiir('lastName');
        ?>
    </label>
    <label>
        email<sup>*</sup> <input type="text" name="email" value="<?php echo inputValue('email') ?>" placeholder="email@cim.hu">
        <?php
        //mezőhiba
        echo hibaKiir('email');
        ?>
    </label>
    <label>
        officecode<sup>*</sup> <input type="text" name="officeCode" value="<?php echo inputValue('officeCode') ?>" placeholder="1-8">
        <?php
        //mezőhiba
        echo hibaKiir('officeCode');
        ?>
    </label>
    <label>
        extension <input type="text" name="extension" value="<?php echo inputValue('extension') ?>" placeholder="x1001">

    </label>
    <label>
        reportsto <input type="text" name="reportsTo" value="<?php echo inputValue('reportsTo') ?>" placeholder="1002">
        <?php
        //mezőhiba
        echo hibaKiir('reportsTo');
        ?>
    </label>
    <label>
        jobtitle <input type="text" name="jobTitle" value="<?php echo inputValue('jobTitle') ?>" placeholder="boss">

    </label>
    <button>Mehet</button>
</form>
</body>
</html><?php
/**
 * eljárás szöveges tipusu mezőértékek visszadására
 * @param $fieldname
 * @return mixed
 * @todo: checkbox?, radio?, select-option?
 */
function inputValue($fieldname)
{
    $ret = '';
    $ret = filter_input(INPUT_POST, $fieldname);

    return $ret;
}

/**
 * Saját hibakiíró eljárás
 * @param $fieldName - mező neve name="fieldname"
 * @return mixed - ha nincs ilyen akkor false, egyébként string:hibaüzenet
 */
function hibaKiir($fieldName){
    global $hiba;//'lássa' az eljárás a változót
    if(isset($hiba[$fieldName])){
        return $hiba[$fieldName];
    }
    return false;
}
