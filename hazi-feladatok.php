<?php
//házi feladatok
/*
1. Írjon egy php programot, amely kiszámolja és kiírja a 2 m élhosszúságú kocka felületét.
V=a*a*6
*/
$a = 23.12;
$felulet = $a*$a*6;
echo "Egy {$a}m oldalú kocka felülete: $felulet m<sup>2</sup>";

//....
/*
8.Írjon egy php programot, amellyel a HTML szót kiíratja 7-es mérettel, piros színnel és Arial betűtípussal.
*/
echo '<div style="font-size:7pt;color:red">HTML</div>';