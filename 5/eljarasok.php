<?php
//saját eljárás készítése
/*
function eljarasNeve(param1, [param2 = 'valami']){ itt param1 kötelező, param2 pedig opcionális default ->'valami'
  //programrészlet, eléri a paramétereket a felsorolt néven
 }
eljerasNeve('param');//eljéréson belül: param1 = 'param',param2 = 'valami'
eljarasNeve('érték1', 'uj érték');param1 = 'érték1',param2 = 'uj éréték'
eljarasNeve(); -> hiba!
lehet
    :void -nincs visszatérése ^^
    return után egy értékkel visszatérˇˇ
function eljarasNeve2([params]){
        //kódrészlet
    $ret ...

    return $ret;//visszatési érték
}
 */
//generáljunk 5 egyedi számot egy tömbbe emelkedő érték sorrendbe 1-100 között, 5 elemet

$szamok = [];//ide gyüjtjük a számokat
$db = 5;//ennyi szám kell
$limit = 10; //1 és limit közötti számokat szeretnénk
for ($i = 1; $i <= $db; $i++) {
    $szamok[] = rand(1, $limit);
}
sort($szamok);//rendezés
echo '<pre>' . var_export($szamok, true) . '</pre>';
//a fenti mintában vannak ismétlődések
$szamok = []; //redeclare, override
for ($i = 1; $i <= $db; $i++) {
    $szamok[] = rand(1, $limit);
}
//array_unique(tömb)
$szamok = array_unique($szamok);
echo '<pre>' . var_export($szamok, true) . '</pre>';
//így nem biztos hogy elég az elemszámnyi generálás
$szamok = [];
for ($i = 1; count($szamok) < $db; $i++) {
    $szamok[] = rand(1, $limit);
    //ismétlődés kiiktatása
    $szamok = array_unique($szamok);
}
echo '<pre>' . var_export($szamok, true) . $i . '</pre>';

$szamok = generalas();
echo '<pre>' . var_export($szamok, true) . '</pre>';
//ugyanez while ciklussal, saját eljárásban
/**
 * Véletlenszám generáló eljárás
 * @todo ...ezt kellene még csinálni
 * @param int $db
 * @param int $limit
 * @return array
 * @author George Horvath
 */
function generalas($db = 5, $limit = 100)
{
    //global $limit;//limit változót látni fogja az eljárás
    $szamok = [];
    //ha a kapott db paraméter érték nagyobb mint a limit akkor végtelen ciklusba futnánk
    if($db>$limit){
        trigger_error('Az eljárást nem jól paraméterezted...');//kód itt megáll...
    }else {
        while (count($szamok) < $db) {
            $szamok[] = rand(1, $limit);
            //ismétlődés kiiktatása
            $szamok = array_unique($szamok);
        }
        sort($szamok);
    }
    //visszatérünk a $szamok tömbbel
    return $szamok;
}

echo '<h2>A generált lotto számok (5/90): '.implode(', ' , generalas(5,90) ).'</h2>';
$db = 7;
$limit = 35;
$szamok = generalas($db,$limit);
echo "<h2>A generált lotto számok ($db/$limit): ".implode(', ' , $szamok )."</h2>";