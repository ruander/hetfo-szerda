<?php
//automárkák select option segédtömbje
$automarkak = [
    'mercedes' => 'Merci',
    'tesla' => 'Tesla',
    'opel' => 'Opel',
    'ford' => 'Ford',
    'Volvo' => 'Volvo'
];
//segédtömb a megszólításokhoz
$genders = [
    'female' => 'úrhölgy',
    'male' => 'úr',
    'other' => 'cimzett'
];
//előfizetések segédtömbje
$prepaid = [
    3 => 'negyedéves',
    6 => 'féléves',
    12 => 'éves'
];
if (!empty($_POST)) {
    $hiba = [];//ide kell gyűjteni a hibákat
    echo '<pre>' . var_export($_POST, true) . '</pre>';
    //hibakezelés és hiba kiírása
    /*
     * név: minimum 3 karakter mb_strlen
     */
    $nev = filter_input(INPUT_POST, 'nev');
    if (mb_strlen($nev) < 3) {
        $hiba['nev'] = '<span class="error">Min. 3 karakter!</span>';
    }

    /*
    * szövegdoboz, max 400 karakter, de minimum 20 mb_strlen és < vagy >
    * kedvenc automárka, kötelező kiválasztás - nem lehet ''*/
    $auto = filter_input(INPUT_POST, 'auto');
    if ($auto == '') {
        $hiba['auto'] = '<span class="error">Válasszon automárkát!</span>';
    }
    /*
     * adatvédelmet kötelező kijelölni - postban kell legyen
     * */
    $terms = filter_input(INPUT_POST, 'terms', FILTER_VALIDATE_INT);
    if (!$terms) {
        $hiba['terms'] = '<span class="error">Adatvédelmet el kéne olvasni!</span>';
    }
    //echo '<pre>' . var_export(gettype($terms), true) . '</pre>';// https://www.php.net/manual/en/function.gettype.php
    /*
     * nem hibakezelés, de a folyamat végén a megszólítás a Nem alapján történjen
     */
    $nem = filter_input(INPUT_POST, 'gender');
    //erősebb ellenőrzés az ellen hogy nem piszkálták meg az ürlap forrását itt, hogy ha megnézzük hogy a kapott érték létezik e mint kulcs a genders segédtömbünkben
    if (!array_key_exists($nem, $genders)) {
        $hiba['gender'] = '<span class="error">Válasszon nemet!</span>';
    }

    /*
     * előfizetés hossza csak 3, 6 vagy 12 lehet, a folyamat végén negyed, fél vagy egyéves
     * */
    $range = filter_input(INPUT_POST, 'range', FILTER_VALIDATE_INT);
    if (!array_key_exists($range, $prepaid)) {
        $hiba['range'] = '<span class="error">A választható előfizetés hoszzok: ' . implode(',', array_keys($prepaid)) . ' hónap!</span>';
    }

    /*
      * kor min 18 év
     */
    $age = filter_input(INPUT_POST, 'age', FILTER_VALIDATE_INT);
    if ($age < 18) {
        $hiba['age'] = '<span class="error">Az ürlapot csak 18 év felettiek tölthetik ki!</span>';
    }
    /*
     * Rendelés dátum csak minimum 24 óra (másnap) - date()
     **/

    $datum = filter_input(INPUT_POST, 'datum');
    $expired = time(); //mostani időbélyegnél legyen nagyobb - ez úgy lehetséges hogy a kiválasztás miatt csak holnapi lesz a jó dátum vagy nagyobb
    $time = strtotime($datum);//időbélyeg - timestamp
    if ($time < $expired) {
        $hiba['datum'] = '<span class="error">Minimum másnapi dátum lehetséges (vagy későbbi)!</span>';
    }
    /*
     * hibátlan űrlap esetén egy táblázatba gyűjtött megjelenítés a body tagben
     * név,kor,üzenet,kedvenc auto,előfizetés hossza, rendelés dátum
     */
    $msg = filter_input(INPUT_POST, 'msg');
    if (mb_strlen($msg) < 20 or mb_strlen($msg) > 400) {
        //hossz nem megfelelő
        $hiba['msg'] = '<span class="error">Min. 20 - max 400 karakter!</span>';
    }


    if (empty($hiba)) {//hibátlan ürlap esetén, adatok tisztázása
        $data = [
            'nev' => $nev,
            'msg' => $msg,
            'auto' => $auto,
            'age' => $age,
            'range' => $range,
            'datum' => date('Y-m-d', $time),
        ];
        //mentsük el egy json formátumba az adatokat
        /*
         {
         property: value,
        property2 : value2,
        }
         */
        $jsonData = json_encode($data);//string - ezt fogjuk eltárolni
        //filenév kialakítása
        $fileName = 'data.json';
        //hf: fopen,fread,fwrite,fclose
        //egyszerű file kiírás /létrehozás-felülírás
        file_put_contents($fileName,$jsonData);//adatok kiírása file-ba
        echo '<pre>' . var_export(json_decode($jsonData,true), true) . '</pre>';
        //table összeállítás
        $table = '<table border="1">';
        foreach ($data as $k => $v) {
            //a plusz vagy különleges kiírások esetén segédváltozóba tesszük az elemeket
            //pl előfizetés esetén nem a $v kell hanem a prepaid tömb értéke
            if($k=='range'){
                //$v override
                $v=$prepaid[$v];
            }
            //megszólítás
            if($k=='nev'){
                $v .= ' '.$genders[$nem].'!';//a $nem változó biztos hogy létezik és jó mert itt hibátlan űrlapon vagyunk
            }
            //egy sor mintájára az elemeink
            $table .= '<tr>
                        <th>' . $k . '</th>
                        <td>' . $v . '</td>
                      </tr>';
        }
        $table .= '</table>';
        echo '<pre>' . var_export($data, true) . '</pre>';

    }
}


?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Űrlapmező típusok</title>
    <style>
        fieldset, fieldset > label, fieldset > div {
            max-width: 450px;
            display: flex;
            flex-flow: column nowrap;
        }

        .error {
            color: red;
            font-size: 13px;
            font-style: italic;
        }
    </style>
</head>
<body>
<?php
//ha van table elemünk akkor írjuk ki azt
if (isset($table)) {
    echo $table;
} else {
    ?>
    <form method="post">
        <fieldset>
            <legend>Dobozcím</legend>
            <label>Név
                <input name="nev" placeholder="John Doe" value="<?php echo inputValue('nev') ?>">
                <?php
                //mezőhiba kiírása
                /*if(isset($hiba['nev'])){
                    echo $hiba['nev'];
                }*/
                echo hibaKiir('nev');

                ?>
            </label>
            <label>
                Hosszabb szövegmező
                <?php echo hibaKiir('msg');//mezőhiba kiírása saját eljárással  ?>
                <textarea name="msg" cols="30" rows="10"
                          placeholder="üzenet..."><?php echo inputValue('msg') ?></textarea>
                <!--nincs value, a tagekbe kell írni a contentet-->
            </label>
            <label>
                Kedvenc autómárka
                <?php echo hibaKiir('auto');//mezőhiba kiírása saját eljárással  ?>
                <select name="auto">
                    <option value="">--válassz--</option>
                    <?php
                    //tömb bejárásával elkészítjük az opciókat
                    $options = '';
                    foreach ($automarkak as $k => $v) {
                        //ha az aktuális elem , benne van a postban akkor selected az elem
                        /*if(filter_input(INPUT_POST,'auto') == $k){
                            $selected = 'selected';
                        }else{
                            $selected = '';
                        }*/
                        //shorten if (feltétel)? true:false
                        $selected = (filter_input(INPUT_POST, 'auto') == $k) ? 'selected' : '';
                        $options .= '<option value="' . $k . '" ' . $selected . '>' . $v . '</option>';
                    }
                    //opciók kiírása
                    echo $options;
                    ?>
                </select>
            </label>
            <label style="padding: 10px 0;display: block">
                <?php echo hibaKiir('terms').'<br>';//mezőhiba kiírása saját eljárással  ?>
                <input type="checkbox" name="terms" value="1"> Elolvastam és megértettem az <a href="#terms">Adatvédelmi
                    irányelveket</a>
            </label>
            <div>
                Nem
                <label><input type="radio" name="gender" value="female"> nő</label>
                <label><input type="radio" name="gender" value="male" checked> férfi</label>
                <label><input type="radio" name="gender" value="other"> egyéb</label>
                <?php echo hibaKiir('gender');//mezőhiba kiírása saját eljárással  ?>
            </div>
            <label>
                ELőfizetés hossza (hó)
                <?php echo hibaKiir('range');//mezőhiba kiírása saját eljárással  ?>
                <input type="range" id="rail" name="range" min="1" max="12" value="<?php echo inputValue('range') ?>">
                <span id="range_counter"><?php echo inputValue('range') ?></span> </label>
            <label>
                Kor
                <input type="number" name="age" min="14" max="99" step="1" value="<?php echo inputValue('age') ?>">
                <?php echo hibaKiir('age');//mezőhiba kiírása saját eljárással  ?>
            </label>
            <label>
                Rendelés dátum
                <?php echo hibaKiir('datum');//mezőhiba kiírása saját eljárással  ?>
                <input type="date" name="datum" value="<?php echo inputValue('datum') ?>">
            </label>

        </fieldset>
        <button>Mehet</button>
    </form>
    <?php
}
?>
<script>
    let range_counter = document.getElementById('range_counter');//doboz
    let rail = document.getElementById('rail');//csúszka
    range_counter.innerText = rail.value;//alap üres esetén a default érték legyen beírva
    //console.log(range_counter,rail.value);
    rail.addEventListener('change', function () {
        //@todo: hf: (js) esemény kezelésében legyen benne hogy hibaüzenetet írunk ki ha nem megfelelő értéken áll (nem 3,6, vagy 12), ahogy a php ellenőrzés, de ez kliens oldal!
        range_counter.innerText = rail.value;
    });
</script>
</body>
</html>
<?php

/**
 * eljárás szöveges tipusu mezőértékek visszadására
 * @param $fieldname
 * @return mixed
 * @todo: checkbox?, radio?, select-option?
 */
function inputValue($fieldname)
{
    $ret = '';

    $ret = filter_input(INPUT_POST, $fieldname);

    return $ret;
}

/**
 * Saját hibakiíró eljárás
 * @param $fieldName - mező neve name="fieldname"
 * @return mixed - ha nincs ilyen akkor false, egyébként string:hibaüzenet
 */
function hibaKiir($fieldName){
    global $hiba;//'lássa' az eljárás a változót
    if(isset($hiba[$fieldName])){
        return $hiba[$fieldName];
    }
    return false;
}
