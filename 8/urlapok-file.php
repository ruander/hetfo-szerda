<?php

$dir ='uploads/';//mappa amibe fel szeretnénk tölteni létezzen ˇˇ
if(!is_dir($dir)){
    mkdir($dir,0755,true);
}
//^^^^
if(!empty($_POST)){
    $hiba=[];//hibaüzenetek tömbje
    $upload = false;
    //var_dump('<pre>',$_FILES);
    $fileToUpload = $_FILES['file_to_upload'];//5 elemű tömb
    if($fileToUpload['error']!==0){
        //most kötelező a file feltöltése
        $hiba['file_to_upload']='baj van';
    }else{
        $upload = true;
        //file műveletet itt ne végezzük el mert nem biztos hogy minden ok az űrlapon
    }

    if(empty($hiba)){
        echo 'nem volt hiba!';
        //ha volt filefeltöltés az ürlapon //ezt fent alakítottuk ki akkor tudunk vele máveleteket végezni
        if($upload === true){
            //ékezettelenítsük és kisbetűsítsük a filenevet, ha nem mi adjuk meg
            $originalFileName = $fileToUpload['name'];
            $goodName = ekezettelenit($originalFileName);
            $filename = $dir.$goodName;
            //mozgassuk a filet az eredeti nevén a kívánt mappába a tmp-ből
            //ha a file feltöltés ereménye volt és létezik
            if(is_uploaded_file($fileToUpload['tmp_name'])
                &&
                move_uploaded_file($fileToUpload['tmp_name'], $filename)
            ) {
                //sikeres filefeltöltés esetén az új néven (ha nem volt jol elnevezve) találjuk a filet
                echo"sikeres feltöltés, file helye a szerveren: <a href='$filename' target='_blank'>$filename</a> !";

            }
        }
    }
}


/*
 * string kezelő eljárások
 */
function ekezettelenit($str) {
    $bad  = array('á','ä','é','í','ó','ö','ő','ú','ü','ű','Á','Ä','É','Í','Ó','Ö','Ő','Ú','Ü','Ű',' ');
    $good = array('a','a','e','i','o','o','o','u','u','u','a','a','e','i','o','o','o','u','u','u','-');
    $str = mb_strtolower($str,"utf-8");//kisbetűsre
    $str = str_replace($bad, $good, $str);//karakterek cseréje
    $str = preg_replace("/[^A-Za-z0-9_.]/", "-", $str);//maradék eltávolítása
    $str = rtrim($str,'-');//végződő - eltávolítása
    return $str;
}


?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Űrlapok - filefeltöltés</title>
</head>
<body>
<form method="post" enctype="multipart/form-data">
    <label>
        File: <input type="file" name="file_to_upload">
        <?php
        if(isset($hiba['file_to_upload'])){
            echo $hiba['file_to_upload'];
        }
        ?>
    </label>
    <button name="submit">Mehet</button>
</form>
</body>
</html>