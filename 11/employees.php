<?php
include "connect.php";//db csatlakozás betöltése -> $link
//Erőforrások
$output = '';//ide gyűjtjük a kimentre írandó elemeket
//action az urlből
$act = filter_input(INPUT_GET, 'action') ?: 'list';//rövid shorten if az igaz ág a condition maga
$tid = filter_input(INPUT_GET, 'tid', FILTER_VALIDATE_INT) ?: null;//ha kapunk számot akkor legyen az, ha nem akkor legyen null
//var_dump($act);
//irodákhoz segédtömb (irodakód - city)
$offices = [];
$qry = "SELECT officeCode,city FROM offices ORDER BY officeCode";
$result = mysqli_query($link, $qry) or die(mysqli_error($link));
while (null !== $row = mysqli_fetch_assoc($result)) {
    $offices[$row['officeCode']] = $row['city'];
}
//var_dump($offices);
//űrlap kezelése
if (!empty($_POST)) {
    $hiba = [];
//employeenumber, szám kell legyen és nem szerepelhet az adatbázisban
    $employeeNumber = filter_input(INPUT_POST, 'employeeNumber', FILTER_VALIDATE_INT);

    if ($employeeNumber < 1) {
        $hiba['employeeNumber'] = '<span class="error">Nem érvényes formátum!</span>';
    } else {
        $qry = "SELECT email FROM employees WHERE employeeNumber  = '$employeeNumber' LIMIT 1";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $row = mysqli_fetch_row($result);
        if (!empty($row) AND $employeeNumber != $tid) {
            $hiba['employeeNumber'] = '<span class="error">Már foglalt azonosító!</span>';
        }
    }

//firstname, (ne maradjon üres)
    $firstName = mysqli_real_escape_string($link, filter_input(INPUT_POST, 'firstName'));
    if ($firstName == '') {
        $hiba['firstName'] = '<span class="error">Kötelező kitölteni!</span>';
    }
//lastname, (ne maradjon üres)
    $lastName = mysqli_real_escape_string($link, filter_input(INPUT_POST, 'lastName'));
    if ($lastName == '') {
        $hiba['lastName'] = '<span class="error">Kötelező kitölteni!</span>';
    }

//extension, -
    $extension = mysqli_real_escape_string($link, filter_input(INPUT_POST, 'extension'));
//email, - legyen email, @todo: a saját emailcíme ne legyen már foglalt email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if ($email === false) {
        $hiba['email'] = '<span class="error">Nem érvényes formátum!</span>';
    } else {//nem szerepelhet a db-ben
        $qry = "SELECT employeeNumber FROM employees WHERE email  = '$email' LIMIT 1";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $row = mysqli_fetch_row($result);
        if (!empty($row)) {
            $hiba['email'] = '<span class="error">Már regisztrált email cím!</span>';
        }
    }
//officecode, szám kell legyen és a db ből kell jönni az adatoknak választható lenyíló menühöz

    $officeCode = filter_input(INPUT_POST, 'officeCode', FILTER_VALIDATE_INT);
    //szerepelnie kell a kapott értéknek a segédtömb kulcsai (index) között

    if (!array_key_exists($officeCode, $offices)) {
        $hiba['officeCode'] = '<span class="error">Hibás irodakód!</span>';
    }
//reportsto, - szám kell legyen (később: szerepelnie kell már, vagy lehet üres is , akkor főnök)
    $reportsTo = filter_input(INPUT_POST, 'reportsTo', FILTER_VALIDATE_INT);
    if ($reportsTo == 0) {
        //@todo: ez csak akkor érdekes ha nem létezik olyan employeenumber mint amit kaptunk
        $reportsTo = null;
        //különben hiba van mert 'piszkálták' a mezőt
    }

//jobtitle -
    $jobTitle = mysqli_real_escape_string($link, filter_input(INPUT_POST, 'jobTitle'));

    //echo '<pre>' . var_export($hiba, true) . '</pre>';
    if (empty($hiba)) {
        //adatok rendberakása egy könnyen kezelhető tömbbe
        $employee = [
            'employeeNumber' => $employeeNumber,
            'firstName' => $firstName,
            'lastName' => $lastName,
            'email' => $email,
            'officeCode' => $officeCode,
            'extension' => $extension,
            'reportsTo' => $reportsTo,
            'jobTitle' => $jobTitle
        ];
        //qry elkészítése uj és update esetre
        if($act == 'create'){
            $qry = "INSERT INTO employees(`" . implode('`,`', array_keys($employee)) . "`)
                VALUES('" . implode("','", $employee) . "')";//kérés összeállítása
        }else{
            echo $qry = "UPDATE employees
                    SET
                    employeeNumber = '{$employee['employeeNumber']}',
                    firstName = '{$employee['firstName']}',
                    lastName = '{$employee['lastName']}',
                    email = '{$employee['email']}',
                    officeCode = '{$employee['officeCode']}',
                    extension = '{$employee['extension']}',
                    reportsTo = '{$employee['reportsTo']}',
                    jobTitle = '{$employee['jobTitle']}'
                    
                    WHERE employeeNumber ='$tid' LIMIT 1";
        }

        //kérés futtatása
        mysqli_query($link, $qry) or die(mysqli_error($link));
        //die('ok');
        //átirányítunk a 10 óra adatbázis nevu filejára ahol ez a lista van
        header('location:' . $_SERVER['PHP_SELF']);
        exit();
    }
}

//CRUD működés
switch ($act) {
    case 'delete':
        //echo 'törlünk: ' . $tid;
        //ha mit törölni, törlünk
        if($tid){
            mysqli_query($link,"DELETE FROM employees WHERE employeeNumber = '$tid' LIMIT 1") or die(mysqli_error($link));
            //átirányítunk listázásra
            header('location:'.$_SERVER['PHP_SELF']);
            exit();
        }
        break;
    case 'update':
        //$output .= '<div><a href="?action=list">&lt;-- vissza</a></div>módosítunk: ' . $tid;
        //adatok lekérése

        if($tid){
            $qry = "SELECT * FROM employees WHERE employeeNumber = '$tid' LIMIT 1";
            $result = mysqli_query($link,$qry) or die(mysqli_error($link));
            $employee = mysqli_fetch_assoc($result);
            //echo '<pre>'.var_export($employee,true).'</pre>';
        }
        //űrlapcím
        $formTitle = "Alkalmazott módosítása --{$employee['employeeNumber']} --";
        //break;
    case 'create':
        //Űrlap új felvitelhez és módosításhoz
        $employee = isset($employee)?$employee:[];//ha nem lenne adat akkor legyen üres $row tömb
        if(!isset($formTitle)) $formTitle = 'Új felvitel';
        $form = '<div><a href="?action=list">&lt;-- vissza</a></div>
                <h1>'.$formTitle.'</h1>
                <form method="post">';//visszagomb , form nyitás, cím
        //employeenumber
        $form .= '<label>
        employeenumber<sup>*</sup> <input type="text" name="employeeNumber"
                                          value="' . inputValue('employeeNumber',$employee) . '" placeholder="5000">' . hibaKiir('employeeNumber') . '</label>';
        //firstname
        $form .= '<label>
        firstname<sup>*</sup> <input type="text" name="firstName" value="';
        $form .= inputValue('firstName',$employee);//mezőérték visszaírása
        $form .= '" placeholder="Doe">';
        //mezőhiba
        $form .= hibaKiir('firstName');
        $form .= '</label>';
        //lastname
        $form .= '<label>
    lastname<sup>*</sup> <input type="text" name="lastName" value="' . inputValue('lastName',$employee) . '" placeholder="Smith">' . hibaKiir('lastName') . '</label>';
        //extension
        $form .= '<label>
    extension <input type="text" name="extension" value="' . inputValue('extension',$employee) . '" placeholder="x1001">
</label>';
        //email
        $form .= '<label>
    email<sup>*</sup> <input type="text" name="email" value="' . inputValue('email',$employee) . '" placeholder="email@cim.org">' . hibaKiir('email') . '</label>';
        //officecode
        $form .= '<label>
    officecode<sup>*</sup>
    <select name="officeCode">
        <option value="0">--válassz--</option>';
        //opciók kialakítása a segédtömb alapján
        foreach ($offices as $oc => $city) {
            $selected = inputValue('officeCode',$employee) == $oc ? 'selected' : '';
            $form .= '<option value="' . $oc . '" ' . $selected . '>' . $oc . ' - ' . $city . '</option>';
        }
        $form .= '</select>' . hibaKiir('officeCode') . '</label>';
        //reportsto
        //választható felhasználók
        $qry = "SELECT employeenumber,firstname,lastname FROM employees";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $options = '<select name="reportsTo">
                        <option value="0">Nincs</option>';
        while (null !== $row = mysqli_fetch_row($result)) {
            $selected = $row[0] == inputValue('reportsTo',$employee) ? 'selected' : '';
            $options .= '<option value="' . $row[0] . '" ' . $selected . '>[' . $row[0] . '] - ' . $row[1] . ' ' . $row[2] . '</option>';
        }
        $options .= '</select>';

        $form .= '<label>
    reportsto ' . $options . hibaKiir('reportsTo') . '</label>';
//jobtitle
        $form .= '<label>
    jobtitle <input type="text" name="jobTitle" value="' . inputValue('jobTitle',$employee) . '" placeholder="boss">
</label>';
        $form .= '<button>Mehet</button>
                </form>';//submit és form zárás
        $output .= $form;
        break;
    default://nincs vagy ismeretlen action paraméter, lista
        //@todo: törlés confirm megvalósítása javascripttel
        //echo 'lista';
        $qry = "SELECT * FROM employees";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $table = '<div><a href="?action=create">Új felvitel</a></div>
                    <table border="1">
                     <tr>
                      <th>Azonosító</th>
                      <th>név</th>
                      <th>email</th>
                      <th>mellék</th>
                      <th>Művelet</th>
                    </tr>';
        while (null !== $row = mysqli_fetch_assoc($result)) {
            //sorok
            $table .= '<tr>
                          <td>' . $row['employeeNumber'] . '</td>
                          <td>' . $row['firstName'] . ' ' . $row['lastName'] . '</td>
                          <td>' . $row['email'] . '</td>
                          <td>' . $row['extension'] . '</td>
                          <td><a href="?action=update&amp;tid=' . $row['employeeNumber'] . '">Módosít</a> | <a href="?action=delete&amp;tid=' . $row['employeeNumber'] . '">Töröl</a></td>
                        </tr>';
        }
        $table .= '</table>';
        $output .= $table;
        break;
}

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Alkalmazott adminisztráció</title>
    <style>
        html, body {
            padding: 0;
            margin: 0;
        }

        label {
            padding: 5px 0;
        }

        form {
            max-width: 500px;
            margin: 0 auto;
            display: flex;
            flex-flow: column nowrap;
        }

        h1 {
            font-family: Arial, sans-serif;
            text-align: center;
        }

        .error {
            font-size: 12px;
            color: red;
            font-style: italic;
        }
    </style>
</head>
<body>
<?php
//a fent kialakított output kiírása
echo $output;

?>
</body>
</html><?php
/**
 * eljárás szöveges tipusu mezőértékek visszadására
 * kiegészítés: ha van adatbázis adat azonos bévvel, akkor ha nincs post visszatér azzal - adatsúlyozás
 * @param $fieldname -- input mező neve
 * @param $row -- asszociatív adatbázisból kapott adattömb
 * @return mixed -- érték
 * @todo: checkbox?, radio?, select-option?
 */
function inputValue($fieldname, $row = [])
{
    //if($row==null) echo "<br>$fieldname";//hibakereséshez kellett
    $ret = '';
    $ret = filter_input(INPUT_POST, $fieldname);
    if($ret !== null){
        return $ret;
    }
    //ha nem tért vissza posttal, nézzük van e db adat, ha van térjünk vissza vele

    if(array_key_exists($fieldname,$row)){
        return $row[$fieldname];
    }
    return '';//$ret;//itt $ret is lehetne mert üres maradt, de most ''
}

/**
 * Saját hibakiíró eljárás
 * @param $fieldName - mező neve name="fieldname"
 * @return mixed - ha nincs ilyen akkor false, egyébként string:hibaüzenet
 */
function hibaKiir($fieldName)
{
    global $hiba;//'lássa' az eljárás a változót
    if (isset($hiba[$fieldName])) {
        return $hiba[$fieldName];
    }
    return false;
}


