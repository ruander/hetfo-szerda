<!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP tanfolyam - ciklusok</title>
</head>
<body>
<?php
//PHP - Personal Home Page
//PHP - PHP hypertext preprocessor - rekurziv elem
/*
for(ciklusváltozó kezdeti értéke; belépési feltétel vizsgálata; ciklusváltozó léptetése){
    ciklusmag
}
 for($i=1;$i<5;$i++){
    //kód
}
 */
for( $i=1 ; $i<=5; $i++ ){
    //írjuk ki az aktuális értékét a ciklusváltozónak
    echo "$i<br>";
}
//példa: 5 menüpont
echo '<ul>';//lista nyitás
//listaelemek
for($i=0;$i<5;$i++){// operátor: ++ értéknövelés 1el; létezik még -- ami csökkent 1el
    echo '<li><a href="?mp='.$i.'">menüpont '.$i.'</a></li>';
}
echo '</ul>';//lista zárás
//while ciklus
/*
while(feltétel){
    //ciklusmag
}
 */
//elöl tesztelés - nem biztos hogy akár 1x lefut
$i = 5;
while($i<5){
    echo $i++."<br>";
    //$i++;
}
echo 'az $i értéke a ciklus után:'.$i.'<br>';
//hátul tesztelés - legalább 1x lefut
$i = 5;
do{
    echo $i++.'<br>';
}while($i<5);

//gyakorlás X*Y táblázat létrehozása
$sor = 8;
$oszlop = 4;
echo '<table border="1">';//table nyitás

for($x=1;$x<=$sor;$x++){//ciklus a soroknak
    echo '<tr>';
    //beágyazott ciklus a celláknak
    for($y=1;$y<=$oszlop;$y++){
        echo "<td>sor: $x | oszlop: $y</td>";
    }
    echo '</tr>';
}

echo '</table>';//table zárás
//színes táblázat
$sor = rand(20,40);
$oszlop = rand(20,40);
echo '<table border="0" cellpadding="0" cellspacing="0" width="400">';//table nyitás

for($x=1;$x<=$sor;$x++){//ciklus a soroknak
    echo '<tr>';
    //beágyazott ciklus a celláknak
    for($y=1;$y<=$oszlop;$y++){
        //cellaszínek generálása cellánként
        $r=rand(0,255);
        $g=rand(0,255);
        $b=rand(0,255);
        echo "<td style='background:rgb($r,$g,$b)'>&nbsp;</td>";
    }
    echo '</tr>';
}

echo '</table>';//table zárás
?>
</body>
</html>