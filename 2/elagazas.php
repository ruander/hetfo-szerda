<!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP tanfolyam - elágazások</title>
</head>
<body>
<?php
/*
 elágazás
if(feltétel){
    //igaz ág
}else{
    //hamis ág
}
 */
$a = true;
if($a == true){// operátor: == -> értékegyezés vizsgálat
    echo 'az $a értéke igaz.';
}else{
    echo 'az $a értéke hamis.';
}
//csonka elágazás, csak igaz ága van, akkor használjuk ha csak a feltétel teljesülésekor szeretnénk végrehajtani vmit egyébként nem
if($a == false){
    echo 'az $a értéke hamis.';
}
//párosság vizsgálata - példa
$dobas = rand(1,6);
if($dobas%2 == 0){
    echo '<div>A dobás értéke '.$dobas.', ami páros</div>';
}else{
    echo '<div>A dobás értéke '.$dobas.', ami páratlan</div>';
}
//short hand vagy shorten vagy rövid if
// (feltétel)?igaz:hamis
echo '<div>A dobás értéke '.$dobas.', ami '.( $dobas%2 == 0 ? 'páros':'páratlan' ).' .</div>';
//több ág
$generaltszam = rand(0,2);
if($generaltszam == 0){
    echo '<div>A generált szám értéke '.$generaltszam.', ami nem páros és nem páratlan.</div>';
}elseif($generaltszam%2 == 0){
    echo '<div>A generált szám értéke '.$generaltszam.', ami páros</div>';
}else{
    echo '<div>A generált szám értéke '.$generaltszam.', ami páratlan</div>';
}

/*
 switch(változó){
    case 'érték':
                    //kód
                break;
    default:
                    //ha egyik case sem jó
                break;
}
 */
$generaltszam = rand(1,6);
echo "<div>A szám: $generaltszam, ami betűvel: ";//tag nyitás
switch($generaltszam){
    case 1:
        echo 'egy';
        break;
    case 2:
        echo 'kettő';
        break;
    case 3:
        echo 'három';
        break;
    default:
        echo 'nagyobb, mint 3';
        break;
}
echo '</div>';//tag zárás
?>

<footer>Ruander Oktatóközpont | PHP tanfolyam | <?php echo date('Y-m-d H:i:s') ?> </footer>
</body>
</html>