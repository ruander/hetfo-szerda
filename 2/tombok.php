<?php
$tomb = [];//üres tömb régebben $tomb = array()
var_dump($tomb);
$tomb = ['alma',true, 45, 43/23];//tömb alap értékekkel
//var_export
echo '<pre>'.var_export($tomb,true).'</pre>';
$tomb = [
  12 => "alma",
  16 => true,
];//vezérelt index
echo '<pre>'.var_export($tomb,true).'</pre>';
$tomb = [
  'username' => 'hgy',
  'email' => 'hgy@ruander.hu',
  'id' => 12312,
    ];
echo '<pre>'.var_export($tomb,true).'</pre>';
//egy elem beszurása irányított indexre
$tomb['password'] = '123456';
$tomb[100] = 'én vagyok a 100 index';
$tomb[] = 'automatikus indexre kerültem';
echo '<pre>'.var_export($tomb,true).'</pre>';
//doksi: https://www.php.net/manual/en/ref.array.php