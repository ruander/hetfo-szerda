<section id="blog" class="grey-bg">
    <div class="wrapper">
        <h1><span class="subtitle">our blog</span>
            recent posts</h1>
        <p class="subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur deleniti ea est ex
            facilis fuga magnam
            non perspiciatis quasi repellendus.</p>
        <ul class="posts flex-container">
            <?php
            //utolsó 3 cikk lekérése
            $qry = "SELECT id,title,seo_title,lead,author,time_published 
            FROM articles 
            ORDER BY time_published DESC 
            LIMIT 3";
            $result = mysqli_query($link,$qry) or die(mysqli_error($link));
            $articles = '';
            //@todo HF: a cikk címére kattintva egy új cikk.php file mutassa az adott cikk tartalmát (extra: a body content része mutassa)
            while( false != $row = mysqli_fetch_assoc($result)){
                $randomNumber = rand(50,150);//komment szám, mert az most nincs, és hogy a dummy kép változzon
                $articles .= '<li class="post">
                <div class="head">
                    <div class="comments">'.$randomNumber.'</div>
                    <img src="https://picsum.photos/480/320?a='.$randomNumber.'" alt="post 1">

                </div>
                <a href="?cikkId='.$row['id'].'" class="title">'.$row['title'].'</a>
                <p>'.$row['lead'].'</p>
                <div class="footer">
                    <div class="author"><img alt="'.$row['author'].'" src="https://picsum.photos/40?c"> '.$row['author'].'</div>
                    <time datetime="'.$row['time_published'].'">'.$row['time_published'].'</time>
                </div>
            </li>';
            }
            echo $articles;//dobozok kiírása
            ?>

            <!--<li class="post">
                <div class="head">
                    <img src="https://picsum.photos/480/320?b" alt="post 2">
                    <div class="comments">13</div>
                </div>
                <a href="#" class="title">lorem ipsum title much much more longer title availability version</a>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad amet atque aut eaque eos id illum,
                    laboriosam perferendis ut? Dicta dignissimos eligendi fuga sed vitae?</p>
                <div class="footer">
                    <div class="author"><img alt="szerző neve" src="https://picsum.photos/40?d"> György Horváth</div>
                    <time datetime="2020-02-26 17:49:00">2020 január 10.</time>
                </div>
            </li>
            <li class="post">
                <div class="head">
                    <img src="https://picsum.photos/480/320?c" alt="post 3">
                    <div class="comments">265</div>
                </div>
                <a href="#" class="title">lorem ipsum title longer version</a>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad amet atque aut eaque eos id illum,
                    laboriosam perferendis ut? Dicta dignissimos eligendi fuga sed vitae?</p>
                <div class="footer">
                    <div class="author"><img alt="szerző neve" src="https://picsum.photos/40?f"> John Doe</div>
                    <time datetime="2020-02-26 17:49:00">2020 február 16.</time>
                </div>
            </li>-->
        </ul>
    </div>
</section>