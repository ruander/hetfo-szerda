<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="?" class="brand-link">
        <img src="https://picsum.photos/100"
             alt="CMS alapok"
             class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">CMS alapok</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="https://picsum.photos/100?random" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block"><?php echo $_SESSION['userdata']['username']; ?></a>
                <a href="?logout">kilépés</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">

            <!-- Add icons to the links using the .nav-icon class
                 with font-awesome or any other icon font library -->
            <?php
            //menü összeállítása a config ban található segédtömb alapján
            $menuHtml = '<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">';
            foreach ($adminMenu as $menuId => $menu) {
                $menuHtml .= '<li class="nav-item">
                        <a href="?p=' . $menuId . '" class="nav-link"><i class="nav-icon ' . $menu["icon"] . '"></i>
                            <p>
                               ' . $menu["title"];
                if ($menu["badgeType"] != "") {
                    $menuHtml .= '<span class="right badge badge-' . $menu["badgeType"] . '">' . $menu["badgeContent"] . '</span>';
                }
                $menuHtml .= '</p>
                        </a>
                     </li>';
            }
            $menuHtml .= '</ul>';
            echo $menuHtml;//menü kiírása
            ?>

        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>