<?php
//@todo: status az űrlapra (checkbox), lastlogin login esetén változzon
//@todo : képfeltöltés lehetősége (profilkép -> adatbázisba mentéssel)
//include "../config/connect.php";//db csatlakozás betöltése -> $link
//védelem önálló futtatás ellen
if (!$link) {
    header('location:index.php');
}
//Erőforrások
$output = '';//ide gyűjtjük a kimentre írandó elemeket
//action az urlből
$act = filter_input(INPUT_GET, 'action') ?: 'list';//rövid shorten if az igaz ág a condition maga
$tid = filter_input(INPUT_GET, 'tid', FILTER_VALIDATE_INT) ?: null;//ha kapunk számot akkor legyen az, ha nem akkor legyen null
$dir = 'images/';//mappa amibe fel szeretnénk tölteni az admin képét
if (!is_dir($dir)) {
    mkdir($dir, 0755, true);
}
//var_dump($act);
$dbTable = 'admins';
//var_dump($offices);
//űrlap kezelése
if (!empty($_POST)) {
    $hiba = [];

//firstname, (ne maradjon üres)
    $username = mysqli_real_escape_string($link, filter_input(INPUT_POST, 'username'));
    if ($username == '') {
        $hiba['username'] = '<span class="error">Kötelező kitölteni!</span>';
    }


//email, - legyen email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if ($email === false) {
        $hiba['email'] = '<span class="error">Nem érvényes formátum!</span>';
    } else {//nem szerepelhet a db-ben
        $qry = "SELECT id FROM $dbTable WHERE email  = '$email' LIMIT 1";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $row = mysqli_fetch_row($result);
        //settype($row[0],'integer');//typecast
        //var_dump($row,$tid);
        if (!empty($row) && $row[0] != $tid) {//ha találunk sort, de nem egyezik meg a módosítandó admin id val
            $hiba['email'] = '<span class="error">Már regisztrált email cím!</span>';
        }
    }
    //jelszót ellenőrizni kell új felvitelkor, illetve módosítás esetén ha az 1 es mezőbe legalább 1 karakter van
    //passwords
    $pass = filter_input(INPUT_POST, 'pass');
    $repass = filter_input(INPUT_POST, 'repass');
    if ($act == 'create' || $pass != '') {

        //min 6 karakter
        if (mb_strlen($pass, 'utf-8') < 6) {
            $hiba['pass'] = '<span class="error">Min 6 karakter!</span>';
        } elseif ($pass != $repass) {
            $hiba['repass'] = '<span class="error">A jelszavak nem egyeztek!</span>';
        } else {
            $pass = password_hash($pass, PASSWORD_BCRYPT);//jelszó kódolása
        }
    }
    //képfeltöltés
    $upload = false;
    //var_dump('<pre>', $_FILES);
    $fileToUpload = $_FILES['file_to_upload'];//5 elemű tömb
    if ($fileToUpload['error'] !== 0) {//nem biztonságos, mert átverhető a file kiterjesztéssel
        //most kötelező a file feltöltése
        //$hiba['file_to_upload'] = 'Nincs file';
    } else {
        //ha kép akkor a getimagesize(filename) visszatérése értelmezhető adathalmazt ad
        $imgInfo = getimagesize($fileToUpload['tmp_name']);
        if (!$imgInfo || !in_array($imgInfo['mime'], $availableImageTypes)) {//operátor -> || : OR ; && : AND
            $hiba['file_to_upload'] = 'Nem jó a file formátuma, csak jpg!';
        } else {
            $upload = true;
        }
    }
    //echo '<pre>' . var_export($hiba, true) . '</pre>';
    if (empty($hiba)) {
        $now = date('Y-m-d H:i:s');
        $status = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT) ?: 0;//ideiglenes status 1
        //adatok rendberakása egy könnyen kezelhető tömbbe
        $admin = [
            'username' => $username,
            'email' => $email,
            'time_created' => $now,
            'password' => $pass,
            'status' => $status,
        ];
        //qry elkészítése uj és update esetre

        if ($act == 'create') {
            $qry = "INSERT INTO $dbTable(`" . implode('`,`', array_keys($admin)) . "`)
                VALUES('" . implode("','", $admin) . "')";//kérés összeállítása
        } else {
            $passUpdate = ($pass != '' ? "password = '$pass', " : "");
            $qry = "UPDATE $dbTable
                    SET
                    username = '{$admin['username']}',
                    email = '{$admin['email']}',
                    $passUpdate
                    time_updated = '$now',
                    status = '{$admin['status']}'                 
                    WHERE id ='$tid' LIMIT 1";
        }

        //kérés futtatása
        mysqli_query($link, $qry) or die(mysqli_error($link));

        //ha volt filefeltöltés az ürlapon //ezt fent alakítottuk ki akkor tudunk vele műveleteket végezni
        if ($upload === true) {
            $fileName = $tid?:mysqli_insert_id($link);//vagy kaptuk urlből, vagy az utoljára beillezstett autoincrement id a queryből
            //kép adatai:
            $width = $imgInfo[0];
            $height = $imgInfo[1];
            $ratio = $width / $height;// < 1 álló, 1 négyzet, > 1 fekvő
            //var_dump($width, $height, $ratio);
            //vágjunk a kép közepéből egy lehető legnagyobb infot adó 150x150 es thumbnailt
            $targetSize = $targetWidth = $targetHeight = 150;//mert negyzet thumbnail és nem írom át a mératarányos elemeket
            //vászonméret kiszámítása
            if ($ratio < 1) {
                //álló kép
                $thumbWidth = $targetWidth;
                $thumbHeight = round($thumbWidth / $ratio);
                $offset_x = 0;
                $offset_y = ($targetHeight - $thumbHeight) / 2;
                //var_dump($offset_x,$offset_y);
            } else {
                //fekvő vagy négyzet
                $thumbHeight = $targetHeight;
                $thumbWidth = round($targetWidth * $ratio);
                $offset_y = 0;
                $offset_x = ($targetWidth - $thumbWidth) / 2;
            }
            //var_dump($targetWidth, $targetHeight);
            $canvas = imagecreatetruecolor($targetWidth, $targetHeight);//vászon létrehozása
            $src_image = imagecreatefromjpeg($fileToUpload['tmp_name']);//eredeti kép memóriába
            imagecopyresampled($canvas, $src_image, $offset_x, $offset_y, 0, 0, $thumbWidth, $thumbHeight, $width, $height);
            //header('Content-type:image/jpeg');
            //a file neve legyen az eredeti ékezet nélküli név, az images mappában

            $fileName = $dir . $fileName . '.jpg';
            imagejpeg($canvas, $fileName, 80);
            //exit();
            //takarítás
            imagedestroy($canvas);
            imagedestroy($src_image);
        }//end ($upload === true)
        //átirányítunk a 10 óra adatbázis nevu filejára ahol ez a lista van
        header('location:' . $baseUrl);
        exit();
    }
}

//CRUD működés
//@todo hf: form integrálása az adminlte klasszokkal
switch ($act) {
    case 'delete':
        //echo 'törlünk: ' . $tid;
        //ha mit törölni, törlünk
        if ($tid) {
            mysqli_query($link, "DELETE FROM $dbTable WHERE id = '$tid' LIMIT 1") or die(mysqli_error($link));
            //átirányítunk listázásra
            header('location:' . $baseUrl);
            exit();
        }
        break;
    case 'update':
        //$output .= '<div><a href="?action=list">&lt;-- vissza</a></div>módosítunk: ' . $tid;
        //adatok lekérése

        if ($tid) {
            $qry = "SELECT * FROM $dbTable WHERE id = '$tid' LIMIT 1";
            $result = mysqli_query($link, $qry) or die(mysqli_error($link));
            $admin = mysqli_fetch_assoc($result);
            //echo '<pre>'.var_export($admin,true).'</pre>';
        }
        //űrlapcím
        $formTitle = "Adminisztrátor módosítása --{$admin['id']} --";
    //break;
    case 'create':
        //Űrlap új felvitelhez és módosításhoz
        $admin = isset($admin) ? $admin : [];//ha nem lenne adat akkor legyen üres $row tömb
        if (!isset($formTitle)) $formTitle = 'Új felvitel';
        $form = '<div><a href="' . $baseUrl . '&amp;action=list">&lt;-- vissza</a></div>
                <h1>' . $formTitle . '</h1>
                <form method="post" enctype="multipart/form-data">';//visszagomb , form nyitás, cím
        //username
        $form .= '<label>
        username<sup>*</sup> <input type="text" name="username" value="';
        $form .= inputValue('username', $admin);//mezőérték visszaírása
        $form .= '" placeholder="Boss">';
        //mezőhiba
        $form .= hibaKiir('username');
        $form .= '</label>';
        //email
        $form .= '<br><label>
    email<sup>*</sup> <input type="text" name="email" value="' . inputValue('email', $admin) . '" placeholder="email@cim.org">' . hibaKiir('email') . '</label>';
        //password
        $form .= '<br><label>
    jelszó<sup>*</sup> <input type="password" name="pass" value="">' . hibaKiir('pass') . '</label>';
        //password újra
        $form .= '<br><label>
    jelszó újra<sup>*</sup> <input type="password" name="repass" value="">' . hibaKiir('repass') . '</label>';
        //státusz
        //ha volt post és nincs a tömbjében akkor tuti nem kell kipipálni
        //@todo ezt egyszerűsíteniˇˇˇˇˇˇ
        if (!empty($_POST)) {
            if (filter_input(INPUT_POST, 'status') == false) {
                $checked = '';
            } else {
                $checked = 'checked';
            }
        } else {
            if (isset($admin['status']) and $admin['status'] == 1) {
                $checked = 'checked';
            } else {
                $checked = '';
            }
        }
        //^^^^^^^^^^^^

        $form .= '<br><label>
        Státusz <input type="checkbox" name="status" value="1" ' . $checked . '></label>';
        //képfeltöltés
        $form .= '<br><label>
        File: <input type="file" name="file_to_upload">';
        $form .= hibaKiir('file_to_upload');
        $form .= '</label>';

        $form .= '<br><button>Mehet</button>
                </form>';//submit és form zárás
        $output .= $form;
        break;
    default://nincs vagy ismeretlen action paraméter, lista
        //@todo: törlés confirm megvalósítása javascripttel
        //echo 'lista';
        $qry = "SELECT * FROM $dbTable";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $table = '<div class="row">
                    <div class="col-2 offset-10">
                        <a class="btn btn-block btn-primary" href="' . $baseUrl . '&amp;action=create">Új felvitel</a>
                    </div>
                  </div>
                  <div class="row">
                   <div class="col-12 pt-2">
                    <table class="table table-striped table-responsive-sm">
                     <tr>
                      <th>Azonosító</th>
                      <th>Kép</th>
                      <th>név</th>
                      <th>email</th>
                      <th>Művelet</th>
                    </tr>';
        while (null !== $row = mysqli_fetch_assoc($result)) {
            //sorok
            $table .= '<tr>
                          <td>' . $row['id'] . '</td>
                          <td>' . getAdminAvatarHtml($row['id']). '</td>
                          <td>' . $row['username'] . '</td>
                          <td>' . $row['email'] . '</td>
                          <td><a class="btn btn-warning btn-xs" href="' . $baseUrl . '&amp;action=update&amp;tid=' . $row['id'] . '">Módosít</a> <a class="btn btn-danger btn-xs" href="' . $baseUrl . '&amp;action=delete&amp;tid=' . $row['id'] . '">Töröl</a></td>
                        </tr>';
        }
        $table .= '</table>
                </div>
               </div>';
        $output .= $table;
        break;
}
//echo $output;//kimenet kiírása
function getAdminAvatarHtml($id){
    $file = 'images/'.$id.'.jpg';
    $ret = '<img class="avatar mini" src="'.$file.'" alt="avatar">';
    return $ret;
}