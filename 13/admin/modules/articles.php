<?php
//@todo, kép feltöltés lehetősége public/images/articles/{cikkid}/...
//védelem önálló futtatás ellen
if (!$link) {
    header('location:index.php');
    exit();
}
//Erőforrások
$output = '';//ide gyűjtjük a kimentre írandó elemeket
//action az urlből
$act = filter_input(INPUT_GET, 'action') ?: 'list';//rövid shorten if az igaz ág a condition maga
$tid = filter_input(INPUT_GET, 'tid', FILTER_VALIDATE_INT) ?: null;//ha kapunk számot akkor legyen az, ha nem akkor legyen null
//var_dump($act);
$dbTable = 'articles';
//var_dump($_POST);

//űrlap kezelése
if (!empty($_POST)) {
    $hiba = [];
    //cím kötelező
    //title
    $title = filter_input(INPUT_POST, 'title');
    $title = strip_tags($title);//távolítsuk el az esetleg TAGeket
    $title = mysqli_real_escape_string($link, $title);//mysqli injection elleni védelem
    if ($title == '') {
        $hiba['title'] = '<div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h5><i class="icon fas fa-ban"></i> Hiba!</h5>Adj meg címet!</div>';
    } else {
        //seo_title itt mindig a cimből készítjük a példában
        $seo_title = ekezettelenit($title);
    }

    //lead
    $lead = mysqli_real_escape_string($link, filter_input(INPUT_POST, 'lead'));
    if (mb_strlen($lead, 'utf-8') > 400) {
        $hiba['lead'] = '<div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h5><i class="icon fas fa-ban"></i> Hiba!</h5>Max 400 karakter lehet a vezér!</div>';
    }
    //content
    $content = mysqli_real_escape_string($link, filter_input(INPUT_POST, 'content'));
    //author
    $author = mysqli_real_escape_string($link, filter_input(INPUT_POST, 'author'));

    //time_published
    $time_published = mysqli_real_escape_string($link, filter_input(INPUT_POST, 'time_published'));

    //echo '<pre>' . var_export($hiba, true) . '</pre>';
    if (empty($hiba)) {
        $now = date('Y-m-d H:i:s');
        $status = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT) ?: 0;
        //adatok rendberakása egy könnyen kezelhető tömbbe
        $data = [
            'title' => $title,
            'seo_title' => $seo_title,
            'lead' => $lead,
            'content' => $content,
            'author' => $author,
            'time_published' => $time_published,
            'status' => $status,
        ];
        //qry elkészítése uj és update esetre

        if ($act == 'create') {
            $data['time_created'] = $now;
            $qry = "INSERT INTO $dbTable(`" . implode('`,`', array_keys($data)) . "`)
                VALUES('" . implode("','", $data) . "')";//kérés összeállítása
        } else {
            //update esetén a data tömb a time updated kell tartalmazza  a time_createdet nem
            $data['time_updated'] = $now;
            //az update mezőket ciklusban állítjuk össze
            $fields = '';
            $fieldElements = [];
            foreach ($data as $fieldName => $fieldData) {
                $fieldElements[] = "$fieldName = '$fieldData'";
            }
            $fields = implode(',', $fieldElements);//az elemeket implodoljuk egy , -vel

            $qry = "UPDATE $dbTable
                    SET
                    $fields      
                    WHERE id ='$tid' LIMIT 1";
        }

        //kérés futtatása
        mysqli_query($link, $qry) or die(mysqli_error($link));

        //átirányítás listázásra
        header('location:' . $baseUrl);
        exit();
    }
}

//CRUD működés

switch ($act) {
    case 'delete':
        //echo 'törlünk: ' . $tid;
        //ha mit törölni, törlünk
        if ($tid) {
            mysqli_query($link, "DELETE FROM $dbTable WHERE id = '$tid' LIMIT 1") or die(mysqli_error($link));
            //átirányítunk listázásra
            header('location:' . $baseUrl);
            exit();
        }
        break;
    case 'update':

        //adatok lekérése

        if ($tid) {
            $qry = "SELECT * FROM $dbTable WHERE id = '$tid' LIMIT 1";
            $result = mysqli_query($link, $qry) or die(mysqli_error($link));
            $row = mysqli_fetch_assoc($result);
            //echo '<pre>'.var_export($row,true).'</pre>';
        }
        //űrlapcím
        $formTitle = "Cikk módosítása --{$row['id']} --";
    //break;
    case 'create':
        //Űrlap új felvitelhez és módosításhoz
        $row = isset($row) ? $row : [];//ha nem lenne adat akkor legyen üres $row tömb
        if (!isset($formTitle)) $formTitle = 'Új felvitel';
        $form = '<div><a href="' . $baseUrl . '&amp;action=list">&lt;-- vissza</a></div>
                <h1>' . $formTitle . '</h1>
                <form method="post">';//visszagomb , form nyitás, cím
        //title
        $form .= '<label>
        Cím<sup>*</sup> <input type="text" name="title" value="';
        $form .= inputValue('title', $row);//mezőérték visszaírása
        $form .= '" placeholder="Cikk címe">';
        //mezőhiba
        $form .= hibaKiir('title');
        $form .= '</label>';
        //seo title
        $form .= '<br><label>
        Seo cím<input type="text" name="seo_title" value="';
        $form .= inputValue('seo_title', $row);//mezőérték visszaírása
        $form .= '" disabled>';
        $form .= '</label>';
        //lead
        $form .= '<br><label>
        Vezér (begépelt karakterek száma)<br><textarea name="lead" rows="5" cols="80" placeholder="max 400 karakteres bevezető szöveg...">' . inputValue('lead', $row) . '</textarea>';
        //mezőhiba @todo, a túllógó szöveg színe legyen piros (400+) VAGY javascriptes karakterszámláló beépítése
        $form .= hibaKiir('lead');
        $form .= '</label>';
        //content
        $form .= '<br><label>
        Tartalom <br><textarea name="content" rows="10" cols="80" placeholder="cikk tartalma...">' . inputValue('content', $row) . '</textarea>';
        $form .= '</label>';
        //author
        $author = inputValue('author', $row) ?: $_SESSION['userdata']['username'];//ha nincs akkor legyen a username aki be van lépve
        $form .= '<br><label>
        Szerző <input type="text" name="author" value="';
        $form .= $author;//mezőérték visszaírása
        $form .= '" placeholder="szerző">';
        $form .= '</label>';
        //time_published @todo legyen botstrappes datetime selector
        $now = date('Y-m-d H:i:s');
        $time_published = inputValue('time_published', $row) ?: $now;
        $form .= '<br><label>
        Publikálás ideje <input type="text" name="time_published" value="';
        $form .= $time_published;//mezőérték visszaírása
        $form .= '" placeholder="' . $now . '">';
        $form .= '</label>';

        //státusz
        //ha volt post és nincs a tömbjében akkor tuti nem kell kipipálni
        //@todo ezt egyszerűsíteniˇˇˇˇˇˇ
        if (!empty($_POST)) {
            if (filter_input(INPUT_POST, 'status') == false) {
                $checked = '';
            } else {
                $checked = 'checked';
            }
        } else {
            if (isset($row['status']) and $row['status'] == 1) {
                $checked = 'checked';
            } else {
                $checked = '';
            }
        }
        //^^^^^^^^^^^^

        $form .= '<br><label>
    Státusz <input type="checkbox" name="status" value="1" ' . $checked . '></label>';

        $form .= '<br><button>Mehet</button>
                </form>';//submit és form zárás
        $output .= $form;
        break;
    default://nincs vagy ismeretlen action paraméter, lista
        //@todo: törlés confirm megvalósítása javascripttel
        //echo 'lista';
        $qry = "SELECT * FROM $dbTable";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $table = '<div class="row">
                    <div class="col-2 offset-10">
                        <a class="btn btn-block btn-primary" href="' . $baseUrl . '&amp;action=create">Új felvitel</a>
                    </div>
                  </div>
                  <div class="row">
                   <div class="col-12 pt-2">
                    <table class="table table-striped table-responsive-sm">
                     <tr>
                      <th>Azonosító</th>
                      <th>Cím</th>
                      <th>Szerző</th>
                      <th>Státusz</th>
                      <th>Megjelenés</th>
                      <th>Művelet</th>
                    </tr>';
        while (null !== $row = mysqli_fetch_assoc($result)) {
            //sorok
            $table .= '<tr>
                          <td>' . $row['id'] . '</td>
                          <td title="' . $row['seo_title'] . '">' . $row['title'] . '</td>
                          <td>' . $row['author'] . '</td>
                          <td>' . $row['status'] . '</td>
                          <td>' . $row['time_published'] . '</td>
                          <td><a class="btn btn-warning btn-xs" href="' . $baseUrl . '&amp;action=update&amp;tid=' . $row['id'] . '">Módosít</a> <a class="btn btn-danger btn-xs" href="' . $baseUrl . '&amp;action=delete&amp;tid=' . $row['id'] . '">Töröl</a></td>
                        </tr>';
        }
        $table .= '</table>
                </div>
               </div>';
        $output .= $table;
        break;
}
