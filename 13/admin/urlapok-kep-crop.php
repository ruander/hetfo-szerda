<?php

$dir = 'images/';//mappa amibe fel szeretnénk tölteni létezzen ˇˇ
if (!is_dir($dir)) {
    mkdir($dir, 0755, true);
}
//^^^^

$availableImageTypes = ['image/jpeg', 'image/jpg'/*,'image/png'*/];//ilyen tipusu képeket engedünk

if (!empty($_POST)) {
    $hiba = [];//hibaüzenetek tömbje
    $upload = false;
    //var_dump('<pre>', $_FILES);
    $fileToUpload = $_FILES['file_to_upload'];//5 elemű tömb
    if ($fileToUpload['error'] !== 0) {//nem biztonságos, mert átverhető a file kiterjesztéssel
        //most kötelező a file feltöltése
        $hiba['file_to_upload'] = 'Nincs file';
    } else {
        //ha kép akkor a getimagesize(filename) visszatérése értelmezhető adathalmazt ad
        $imgInfo = getimagesize($fileToUpload['tmp_name']);
        if (!$imgInfo || !in_array($imgInfo['mime'], $availableImageTypes)) {//operátor -> || : OR ; && : AND
            $hiba['file_to_upload'] = 'Nem jó a file formátuma, csak jpg!';
        } else {
            $upload = true;
        }
    }

    if (empty($hiba)) {
        //echo 'nem volt hiba!';
        //ha volt filefeltöltés az ürlapon //ezt fent alakítottuk ki akkor tudunk vele műveleteket végezni
        if ($upload === true) {
            //kép adatai:
            $width = $imgInfo[0];
            $height = $imgInfo[1];
            $ratio = $width / $height;// < 1 álló, 1 négyzet, > 1 fekvő
            //var_dump($width, $height, $ratio);
            //vágjunk a kép közepéből egy lehető legnagyobb infot adó 150x150 es thumbnailt
            $targetSize = $targetWidth = $targetHeight = 150;//mert negyzet thumbnail és nem írom át a mératarányos elemeket
            //vászonméret kiszámítása
            if ($ratio < 1) {
                //álló kép
                $thumbWidth = $targetWidth;
                $thumbHeight = round($thumbWidth/$ratio);
                $offset_x = 0;
                $offset_y = ($targetHeight - $thumbHeight)/2;
                //var_dump($offset_x,$offset_y);
            } else {
                //fekvő vagy négyzet
                $thumbHeight = $targetHeight;
                $thumbWidth = round($targetWidth*$ratio);
                $offset_y = 0;
                $offset_x = ($targetWidth - $thumbWidth)/2;
            }
            //var_dump($targetWidth, $targetHeight);
            $canvas = imagecreatetruecolor($targetWidth, $targetHeight);//vászon létrehozása
            $src_image = imagecreatefromjpeg($fileToUpload['tmp_name']);//eredeti kép memóriába
            imagecopyresampled($canvas, $src_image, $offset_x, $offset_y, 0, 0, $thumbWidth, $thumbHeight, $width, $height);
            //header('Content-type:image/jpeg');
            //a file neve legyen az eredeti ékezet nélküli név, az images mappában

            $fileName = $dir. ekezettelenit($fileToUpload['name']);
            imagejpeg($canvas,$fileName,100);
            //exit();
            //takarítás
            imagedestroy($canvas);
            imagedestroy($src_image);
            //file helye linkkel
            echo "<div>A feltöltött kép: <a href='$fileName' target='_blank'>itt érhető el</a></div>";
        }
    }
}


/*
 * string kezelő eljárások
 */
function ekezettelenit($str)
{
    $bad = array('á', 'ä', 'é', 'í', 'ó', 'ö', 'ő', 'ú', 'ü', 'ű', 'Á', 'Ä', 'É', 'Í', 'Ó', 'Ö', 'Ő', 'Ú', 'Ü', 'Ű', ' ');
    $good = array('a', 'a', 'e', 'i', 'o', 'o', 'o', 'u', 'u', 'u', 'a', 'a', 'e', 'i', 'o', 'o', 'o', 'u', 'u', 'u', '-');
    $str = mb_strtolower($str, "utf-8");//kisbetűsre
    $str = str_replace($bad, $good, $str);//karakterek cseréje
    $str = preg_replace("/[^A-Za-z0-9_.]/", "-", $str);//maradék eltávolítása
    $str = rtrim($str, '-');//végződő - eltávolítása
    return $str;
}


?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Űrlapok - filefeltöltés</title>
</head>
<body>
<form method="post" enctype="multipart/form-data">
    <label>
        File: <input type="file" name="file_to_upload">
        <?php
        if (isset($hiba['file_to_upload'])) {
            echo $hiba['file_to_upload'];
        }
        ?>
    </label>
    <button name="submit">Mehet</button>
</form>
</body>
</html>