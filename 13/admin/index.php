<?php
//erőforrások
include_once "../config/connect.php";//db csatlakozás
include_once "../config/config.php";//környezeti 'adatok,beállítások'
include_once "../config/functions.php";//saját eljárások
//mf indítása:
session_start();
$o = filter_input(INPUT_GET, 'p') ?: 0;//vagy kapunk vagy 0, az most a dashboard
$baseUrl = 'index.php?p=' . $o;//moduleoknak az alap url
//var_dump($_SESSION);//mf tömb
//var_dump(session_id());//mf azonosító
//kiléptetünk ha kell
if (filter_input(INPUT_GET, 'logout') !== null) logout();

$auth = auth();//belépés ellenőrzése
if (!$auth) {//ha nincs érvényes belépés, irány a login
    header('location:login.php');
    exit();
    //echo '<h1>Nincs érvényes belépés</h1>';
}
//van érvényes belépés, modul betöltése
//adminMenu segédtömb felhasználásával

$moduleFile = $moduleDir . $adminMenu[$o]['moduleName'] . $moduleExtension;
if (file_exists($moduleFile)) {//ha létezik a modul töltsük be
    include $moduleFile;//vagy jön innen az output,
} else {//vagy kialakítjuk h nincs ilyen modul
    $output = 'Nincs ilyen modul: ' . $moduleFile;
}

?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Ruander PHP tanfolyam | Adminisztrációs felület | Vezérlőpult</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!--saját stílusok-->
    <link href="css/custom.css" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

    <?php
    //navbar
    /*require_once "includes/navbar.php";*/
    //sidebar - oldalsó menü
    require_once "includes/sidebar.php";
    ?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?php echo $adminMenu[$o]['title'] ?></h1>
          </div>
          <div class="col-sm-6">
            <!--<ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Blank Page</li>
            </ol>-->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Title</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                                title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip"
                                title="Remove">
                            <i class="fas fa-times"></i></button>
                    </div>
                </div>
                <div class="card-body">
                    <?php echo $output;//(modul)kimenet kiíírása ?>
                </div>
                <!-- /.card-body -->
                <!--        <div class="card-footer">
                          Footer
                        </div>-->
                <!-- /.card-footer-->
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="float-right d-none d-sm-block">
            <b>Ruander</b> PHP tanfolyam | CMS alapok
        </div>
        <strong>Copyright &copy; <?php echo date('Y') ?> <a href="htts://ruander.hu" target="_blank">Ruander
                Oktatóközpont</a>.</strong>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="js/adminlte.min.js"></script>
</body>
</html>
