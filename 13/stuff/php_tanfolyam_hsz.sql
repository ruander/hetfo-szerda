-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2020. Máj 27. 20:10
-- Kiszolgáló verziója: 10.4.11-MariaDB
-- PHP verzió: 7.3.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `php_tanfolyam_hsz`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `admins`
--

CREATE TABLE `admins` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(60) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` tinyint(1) UNSIGNED NOT NULL,
  `lastLogin` datetime DEFAULT NULL,
  `time_created` datetime NOT NULL,
  `time_updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `admins`
--

INSERT INTO `admins` (`id`, `username`, `email`, `password`, `status`, `lastLogin`, `time_created`, `time_updated`) VALUES
(1, 'superadmin', 'hgy@iworkshop.hu', '$2y$10$HizlnfpYryhjGsYMm6PDzuErrmeG0qRd.4rasB.zhqgdx5EAscdGq', 1, NULL, '2020-05-20 19:08:13', '2020-05-27 20:06:57'),
(8, 'ujabb teszt4', 't23@tt.tt', '$2y$10$GvmFm4EBWJgTAHxcrOfa6OUK56UT7RpNSAzv/QooCS28638a5BJ9a', 1, NULL, '2020-05-27 19:42:48', '2020-05-27 19:56:49');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `sessions`
--

CREATE TABLE `sessions` (
  `sid` varchar(80) NOT NULL,
  `spass` varchar(80) NOT NULL,
  `stime` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `sessions`
--

INSERT INTO `sessions` (`sid`, `spass`, `stime`) VALUES
('2066ais9a35839lv7btss703mr', '0bb4f6d9165f3618271c29c4e0717f9a', 1590602838),
('kbik5j6eiic9cmabbjnkrtkt6j', '47b9f6c409a1430011e0c06544e7c117', 1590601325),
('lt41m4qm4g06jd4sn3n9g7nmc7', 'ae357412c26188cbd3ff27a021225325', 1590602817);

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- A tábla indexei `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`sid`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
