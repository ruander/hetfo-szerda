##Adminoknak (belépéshez) adatok
###Táblanév: admins
* id - rendszerazonosító - *primary key,auto increment, int(11), unsigned* 
* email - azonosításhoz - *unique,varchar(100),utf8_general_ci*
* jelszó - azonosításhoz - *varchar(100),utf8_general_ci*
* username - megszólításhoz - *varchar(60),utf8_general_ci*
* status - aktív/inaktív - *tinyint(1),unsigned*
* lastLogin - *datetime*
* time_created - *datetime*
* time_updated - *datetime*