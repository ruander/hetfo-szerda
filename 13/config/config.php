<?php
$secret_key = 'S3cr3T_K3y!';//titkosító segédkulcs
$moduleExtension = '.php';//ilyen modul kit. eket használunk
$moduleDir = 'modules/';//module-ok mappája
//menüpontok az admin felülethez
$adminMenu = [
    0 => [
        'title' => 'Vezérlőpult',
        'moduleName' => 'dashboard',
        'icon' => 'fas fa-tachometer-alt',
        'badgeType' => '',
        'badgeContent' => 'új'
    ],
    1 => [
        'title' => 'Cikkek',
        'moduleName' => 'articles',
        'icon' => 'far fa-newspaper',
        'badgeType' => '',
        'badgeContent' => ''
    ],
    2 => [
        'title' => 'Adminisztrátorok',
        'moduleName' => 'admins',
        'icon' => 'fas fa-user',
        'badgeType' => 'danger',
        'badgeContent' => 'admin'
    ]
];
$availableImageTypes = ['image/jpeg', 'image/jpg'/*,'image/png'*/];//ilyen tipusu képeket engedünk a rendszerbe feltölteni