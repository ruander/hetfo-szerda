<?php
//saját eljárásaink
/**
 * Admin beléptető eljárás
 * @return bool
 */
function login()
{
    global $link, $secret_key;//link, secret_key globálissá tétele az eljárás számára lathatóvá teszi
//belépés ellenőrzése
    $password = filter_input(INPUT_POST, 'password');
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if ($email != false) {
        //$password = md5($password);//MOST ALAP MD5 KÓDOLÁST HASZNÁLTUNK, EGY EGYSZERŰ LEKÉRÉSSEL ELLENŐRIZNI TUDUNK !!!LECSERÉLTÜK!!!

//db , lekérés ...
        $qry = "SELECT id,username,email,password FROM admins WHERE email = '$email' AND status = 1 LIMIT 1";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $admin = mysqli_fetch_assoc($result);
//echo '<pre>'.var_export($admin,true).'</pre>';//teszt
        $passVerify = password_verify($password,$admin['password']);
        //var_dump($admin,$passVerify);//jelszó ellenőrzése - új
        if ($passVerify !== false) {
            //mf tömb feltöltése az adatokkal
            $_SESSION['userdata'] = $admin;
            $_SESSION['sid'] = session_id();
            //sid, stime, spass
            $stime = time();
            //spass kialakítása admin_id + mf azonosító
            $spass = md5($admin['id'] . $_SESSION['sid'] . $secret_key);
            //bejelentkezés eltárolása az adatbázisba (sessions tábla)
            //todo: hf: lastlgin dátum beírása az admin recordjába
            //ha lenne beragadt bejelentkezés a mf azonosítóra
            mysqli_query($link, "DELETE FROM sessions WHERE sid = '" . session_id() . "' LIMIT 1");
            $qry = "INSERT INTO sessions(sid,spass,stime)
                      values('{$_SESSION['sid']}','$spass',$stime)";
            mysqli_query($link, $qry) or die(mysqli_error($link));
            return true;
        }
    }
    return false;
}
/**
 * Admin kiléptető eljárás
 *
 * :void
 */
function logout(){
    global $link;
    //töröljük a sessionsből a rekordot
    mysqli_query($link,"DELETE FROM sessions WHERE sid = '".session_id()."' LIMIT 1");
    session_destroy();//mf roncsolása
    $_SESSION = [];//tömb ürítése
}
/**
 * Admin felület belépés érvényesség ellenőrzése
 * @return bool
 */
function auth()
{
    global $link, $secret_key;
    //belépés ellenőrzése

//a jelenlegi mf azonosítóhoz kérjünk le adato(ka)t a sessionsből (spass)
    $sid = session_id();
    $now = time();
    $expired = $now - 600;//10p nél régebbi belépések ne legyenek érvényesek
    $qry = "SELECT spass FROM sessions WHERE sid = '$sid' AND stime > $expired LIMIT 1";
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));
    $row = mysqli_fetch_row($result);
    if (!empty($row)) {//ha nem üres a kapott tömb, akkor volt ilyen mf az.  sessions táblába
        //most történt cselekvés frissitsük az stime mezőt
        mysqli_query($link, "UPDATE sessions SET stime = $now WHERE sid = '$sid' LIMIT 1") or die(mysqli_error($link));//stime update
        //ujra legyártjuk az spasst
        $spass = md5($_SESSION['userdata']['id'] . $sid . $secret_key);//ellenőrzés a db ből kapottal
        if ($spass === $row[0]) {//minden ok van érvényes belépés
            return true;//sikerült ellenőrizni
        }
    }
    return false;
}

/**
 * eljárás szöveges tipusu mezőértékek visszadására
 * kiegészítés: ha van adatbázis adat azonos bévvel, akkor ha nincs post visszatér azzal - adatsúlyozás
 * @param $fieldname -- input mező neve
 * @param $row -- asszociatív adatbázisból kapott adattömb
 * @return mixed -- érték
 * @todo**: checkbox?, radio?, select-option?
 */
function inputValue($fieldname, $row = [])
{
    //if($row==null) echo "<br>$fieldname";//hibakereséshez kellett
    $ret = '';
    $ret = filter_input(INPUT_POST, $fieldname);
    if($ret !== null){
        return $ret;
    }
    //ha nem tért vissza posttal, nézzük van e db adat, ha van térjünk vissza vele

    if(array_key_exists($fieldname,$row)){
        return $row[$fieldname];
    }
    return '';//$ret;//itt $ret is lehetne mert üres maradt, de most ''
}

/**
 * Saját hibakiíró eljárás
 * @param $fieldName - mező neve name="fieldname"
 * @return mixed - ha nincs ilyen akkor false, egyébként string:hibaüzenet
 */
function hibaKiir($fieldName)
{
    global $hiba;//'lássa' az eljárás a változót
    if (isset($hiba[$fieldName])) {
        return $hiba[$fieldName];
    }
    return false;
}

/*
 * string kezelő eljárások
 */
function ekezettelenit($str) {
    $bad  = array('á','ä','é','í','ó','ö','ő','ú','ü','ű','Á','Ä','É','Í','Ó','Ö','Ő','Ú','Ü','Ű',' ');
    $good = array('a','a','e','i','o','o','o','u','u','u','a','a','e','i','o','o','o','u','u','u','-');
    $str = mb_strtolower($str,"utf-8");//kisbetűsre
    $str = str_replace($bad, $good, $str);//karakterek cseréje
    $str = preg_replace("/[^A-Za-z0-9_]/", "-", $str);//maradék eltávolítása
    $str = rtrim($str,'-');//végződő - eltávolítása
    return $str;
}
/*
 * string kezelő eljárások
 */
function ekezettelenitFileName($str) {
    $bad  = array('á','ä','é','í','ó','ö','ő','ú','ü','ű','Á','Ä','É','Í','Ó','Ö','Ő','Ú','Ü','Ű',' ');
    $good = array('a','a','e','i','o','o','o','u','u','u','a','a','e','i','o','o','o','u','u','u','-');
    $str = mb_strtolower($str,"utf-8");//kisbetűsre
    $str = str_replace($bad, $good, $str);//karakterek cseréje
    $str = preg_replace("/[^A-Za-z0-9_.]/", "-", $str);//maradék eltávolítása
    $str = rtrim($str,'-');//végződő - eltávolítása
    return $str;
}
