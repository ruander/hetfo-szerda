<?php
/*
2. Készítsünk programot, amely kiszámolja az első 100 darab. természetes szám összegét, majd kiírja az eredményt. (Az összeg kiszámolásához vezessünk be egy változót, amelyet a program elején kinullázunk, a ciklusmagban pedig mindig hozzáadjuk a ciklusváltozó értékét, tehát sorban az 1, 2, 3, 4, ..., 100 számokat.)
 */
$sum = 0;
for($i=1;$i<=100;$i++){
    $sum += $i;//$sum = $sum + $i
}
echo "<div>Az 1-100 tartó pozítív egészek összege: $sum</div>";
/*
 3. Készítsünk programot, amely kiszámolja az első 7 darab. természetes szám szorzatát egy ciklus segítségével. (A szorzat kiszámolásához vezessünk be egy változót, amelyet a program elején beállítunk 1-re, a ciklusmagban pedig mindig hozzászorozzuk a ciklusváltozó értékét, tehát sorban az 1, 2, 3, ..., 7 számokat.)
 */
$mtpl = 1;//ide gyűjtjük a szorzatot
for($i=1;$i<=7;$i++){
    $mtpl *= $i;//$mtpl = $mtpl * $i
}
echo "<div>Az 1-7 tartó pozítív egészek szorzata: $mtpl</div>";