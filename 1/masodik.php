<?php
//pure php file
$veletlenSzam = rand(1,6);
echo $veletlenSzam;
echo '<br>';
echo getrandmax();//rendszer legnagyobb egész szám amit a rand eljárással generálni tudunk
echo '<br>';
echo '<h2>A generált szám:';
echo $veletlenSzam;
echo '</h2>';
//konkatenáció - összefűzés
echo '<h2>A generált szám:'.$veletlenSzam.'</h2>';//operátor: . összefűzi a jobb és bal oldalán található elemeket - string
$szam2 = rand(0,20);
//műveletek
$osszeg = $szam2 + $veletlenSzam;// matematikai operátorok: + - / * %->maradékos osztás
echo 'Az összeg: '.$osszeg.'<br>';
echo "Az összeg: $osszeg<br>";
echo 'Az összeg: $osszeg<br>';
echo "Az összeg: \$osszeg<br>";// operátor: \ -> escape az utána következő karaktert kiveszi a végrehajtható PHP elemek közül
echo 'Peter O\'Toole';
echo " ezt mondta: \"idézet\" - és igaza volt";
//gyakorlás
//feladat, egy 100x100 as div aminek  a háttere szürkeárnyalatokban változik - minden betöltéskor generálódik a színe
$szurke = rand(0,255);
$myDiv = '<div style="width:100px;height:100px;background:rgb('.$szurke.','.$szurke.','.$szurke.')">teszt div</div>';
echo $myDiv;
//szines doboz
$red = rand(0,255);
$green = rand(0,255);
$blue = rand(0,255);
$myDiv = '<div style="width:100px;height:100px;background:rgb('.$red.','.$green.','.$blue.')">teszt div</div>';//újradeklarálás
echo $myDiv;