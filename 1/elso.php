<!doctype html>
<html lang="hu">
<head>
 <title>PHP tanfolyam - alapok</title>
 <meta charset="utf-8">
</head>
<body>
<?php 
//php egysoros megjegyzés
/*
több soros
megjegyzés
*/
print "Hello PHP! ";//stdo standard output - maga a file - operátor: "" vagy '' string operátor
echo '<br>A nevem Horváth <b>György</b>.';
//változók
/*
$valtozoneve : operátor $ - változó
$egeszszam
$egesz_szam | snake case
$egeszSzam | camel case
Fontos: nem kezdődhet számmal  -> $2labda
*/
$szoveg = "Ez egy teszt szöveg";//operátor: = -> értékadó operátor, tipus: string
$egeszSzam = 23;//tipus int vagy integer
$logikai = true;// tipus: bool vagy boolean
$lebegoPontos = 56/23;//float vagy floating point
$szupertitkos_szerver_info = "secret";
//változó adatainak kiírása, fejlesztés idejére csak
echo '<pre>';
var_dump($szoveg, $egeszSzam, $logikai, $lebegoPontos);
echo '</pre>';
$nev = "Horváth <b>György</b>";
?>
<pre>
he
				llo
	 world by pre
</pre>
<br>Hello <?php echo $nev; ?>!
</body>
</html>